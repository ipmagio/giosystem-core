"""Fetch inputs to copsystem packages from the archive

This script can be used in order to initialize a local processing chain.
"""

from __future__ import division
import os.path
import logging
import datetime
import time
import argparse

from giosystemcore.settings import get_settings
from giosystemcore.packages.packagefactory import get_package
from giosystemcore.hosts.hostfactory import get_host

logger = logging.getLogger(__name__)


def _convert_to_datetime(expr):
    return datetime.datetime.strptime(expr, "%Y-%m-%dT%H:%M:%S")


def _convert_to_log_level(expr):
    return {
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warning": logging.WARNING,
        "error": logging.ERROR,
        "critical": logging.CRITICAL,
    }.get(expr.lower(), logging.DEBUG)


def build_parser():
    description = """
    Initialize the system by getting some inputs from the archive
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("settings_url")
    parser.add_argument("start_timeslot", type=_convert_to_datetime)
    parser.add_argument("package_names", nargs="+")
    parser.add_argument("--hours", default=24, type=int,
                        help="defaults to %(default)s")
    parser.add_argument("--log-level", default="warning",
                        type=_convert_to_log_level,
                        help="defaults to %(default)s")
    return parser


def get_package_inputs(start_timeslot, settings_url, package_name="",
                       num_hours=1):
    get_settings(settings_url, initialize_logging=False)
    package = get_package(package_name, start_timeslot)
    host = get_host()
    for hour in range(num_hours):
        logger.info("Processing hour {:02d}".format(hour))
        ts = start_timeslot + datetime.timedelta(hours=hour)
        package.timeslot = ts
        for inp in package.inputs:
            destination = os.path.join(host.data_dirs[0], inp.search_path)
            logger.debug("destination: {}".format(destination))
            retrieved = inp.fetch(
                destination, use_io_buffer=False,
                use_data_receiver=False, use_archive=True,
                decompress=False
            )
            time.sleep(0.01)


def main():
    parser = build_parser()
    args = parser.parse_args()
    logging.basicConfig(level=args.log_level)
    for name in args.package_names:
        get_package_inputs(args.start_timeslot, args.settings_url, name,
                           args.hours)


if __name__ == "__main__":
    main()

