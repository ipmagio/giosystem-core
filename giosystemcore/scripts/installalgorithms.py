'''
A script to automate the installation of the giosystem's core algorithms.
'''

import os
import argparse

import giosystemcore.settings
from giosystemcore.hosts import hostfactory
from giosystemcore.packages import packagefactory

GIOSYSTEM_ALGORITHMS = {
    'cma_g2': {
        'url': 'https://bitbucket.org/ipmagio/cma_g2/get/v3.3.tar.gz',
        'sample_package': 'process_goes_cma',
        'function': 'install_cma',
    },
    'lrit2hdf5_g2': {
        'url': 'https://bitbucket.org/ipmagio/lrit2hdf5_g2/get/v2.5.tar.gz',
        'sample_package': 'preprocess_goes_lrit',
        'function': 'install_lrit2hdf5',
    },
    'hmwr_hrit2hdf5': {
        'url': 'https://bitbucket.org/ipmagio/hmwr_hrit2hdf5/get/v1.2.tar.gz',
        'sample_package': 'preprocess_himawari_hrit',
        'function': 'install_himawari_hrit2hdf5',
    },
    'sa_g2': {
        'url': 'https://bitbucket.org/ipmagio/sa_g2/get/v2.1.tar.gz',
        'sample_package': 'process_goes_solar_angles',
        'function': 'install_sa',
    },
    'ref_g2': {
        'url': 'https://bitbucket.org/ipmagio/ref_g2/get/v1.1.tar.gz',
        'sample_package': 'process_goes_ref',
        'function': 'install_ref',
    },
    'sat_data_stat_g2': {
        'url': 'https://bitbucket.org/ipmagio/sat_data_stat_g2/get/v4.1.tar.gz',
        'sample_package': 'process_goes_sat_data_stat',
        'function': 'install_sat_data_stat',
    },
    'lst_g2': {
        'url': 'https://bitbucket.org/ipmagio/lst_g2/get/v3.5.tar.gz',
        'sample_package': 'process_goes_lst',
        'function': 'install_lst',
    },
    'lst10_g2': {
        'url': 'https://bitbucket.org/ipmagio/lst10/get/v2.0.tar.gz',
        'sample_package': 'process_lst10dailyCycle_hourly',
        'function': 'install_lst10',
    },
    'timemerge_g2': {
        'url': 'https://bitbucket.org/ipmagio/timemerge_gldata/get/v1.0.tar.gz',
        'sample_package': 'process_lst10dailyCycle',
        'function': 'install_timemerge',
    },
    'ngp2grid_g2': {
        'url': 'https://bitbucket.org/ipmagio/ngp2grid_g2/get/v5.2.tar.gz',
        'sample_package': 'lst_merge',
        'function': 'install_ngp2grid',
    },
    'grib2hdf5_g2': {
        'url': 'https://bitbucket.org/ipmagio/grib2hdf5_g2/get/v2.1.tar.gz',
        'sample_package': 'preprocess_goes_grib',
        'function': 'install_grib2hdf5',
    },
}

def build_parser():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('settings_url', help='The full URL to the giosystem'
                        'settings app. Example: http://gio-gl.meteo.pt/'
                        'giosystem/coresettings/api/v1/')
    parser.add_argument('bitbucket_user', help='Name of the bitbucket '
                        'account that is authorized to access the algorithm '
                        'packages. This information is available at IPMA\'s '
                        'GIO-GL internal docs.')
    parser.add_argument('bitbucket_password', help='Password of the bitbucket '
                        'account that is authorized to access the algorithm '
                        'packages.')
    parser.add_argument('-p', '--package', help='Name of the package to '
                        'install.', action='append',
                        choices=GIOSYSTEM_ALGORITHMS.keys())
    return parser

def install_cma(gio_host, user, password):
    _install_package('cma_g2', gio_host, user, password)

def install_lrit2hdf5(gio_host, user, password):
    _install_package('lrit2hdf5_g2', gio_host, user, password)

def install_himawari_hrit2hdf5(gio_host, user, password):
    _install_package('hmwr_hrit2hdf5', gio_host, user, password)

def install_grib2hdf5(gio_host, user, password):
    _install_package('grib2hdf5_g2', gio_host, user, password)

def install_sa(gio_host, user, password):
    _install_package('sa_g2', gio_host, user, password)

def install_ref(gio_host, user, password):
    _install_package('ref_g2', gio_host, user, password)

def install_sat_data_stat(gio_host, user, password):
    _install_package('sat_data_stat_g2', gio_host, user, password)

def install_lst(gio_host, user, password):
    _install_package('lst_g2', gio_host, user, password)

def install_lst10(gio_host, user, password):
    _install_package('lst10_g2', gio_host, user, password)

def install_timemerge(gio_host, user, password):
    _install_package('timemerge_g2', gio_host, user, password)

def install_ngp2grid(gio_host, user, password):
    _install_package('ngp2grid_g2', gio_host, user, password)

def _install_package(package_key, gio_host, user, password):
    install_path = os.path.join(gio_host.code_dir, 'build')
    if not os.path.isdir(install_path):
        os.makedirs(install_path)
    print("install_path: {}".format(install_path))
    gio_host.run_program('wget --user={} --http-password={} {}'.format(
                         user,
                         password,
                         GIOSYSTEM_ALGORITHMS[package_key]['url']),
                         working_directory=install_path)
    archive = [i for i in os.listdir(install_path)][0]
    gio_host.run_program('tar --gunzip --extract --verbose --file {}'.format(
                         archive), working_directory=install_path)
    gio_host.run_program('rm -rf {}'.format(archive),
                         working_directory=install_path)
    new_dir = os.listdir(install_path)[0]
    code_dir = os.path.join(install_path, new_dir)
    adapt_makefile(code_dir, gio_host.code_dir)
    gio_host.run_program('make', working_directory=code_dir, shell=True)
    gio_host.run_program('make install', working_directory=code_dir,
                         shell=True)
    gio_host.run_program('rm -rf {}'.format(install_path))
    get_static_files(package_key, gio_host)

def adapt_makefile(code_dir, host_code_dir):
    '''
    '''

    new_contents = []
    with open(os.path.join(code_dir, 'Makefile.template')) as fh:
        for line in fh:
            if line.startswith('GEOLAND_LIBS'):
                new_line = 'GEOLAND_LIBS = %s\n' % host_code_dir
            elif line.startswith('OUTPUT_PATH'):
                new_line = 'OUTPUT_PATH = %s\n' % os.path.join(host_code_dir,
                                                               'bin')
            else:
                new_line = line
            new_contents.append(new_line)
    with open(os.path.join(code_dir, 'Makefile'), 'w') as fh:
        fh.writelines(new_contents)

def get_static_files(package_key, gio_host):
    # we arbitrarily choose the 201301010000 timeslot because we only care
    # about getting the static inputs that the package defines, and these
    # do not depend on the timeslot
    pack = packagefactory.get_package(
        GIOSYSTEM_ALGORITHMS[package_key]['sample_package'],
        '201301010000'
    )
    for inp in pack.inputs:
        if inp.frequency == 'static' and inp.is_system_input:
            found_host, found_paths = inp.find()
            if found_host is not gio_host:
                target_dir = os.path.join(gio_host.data_dirs[0],
                                          inp.search_path)
                fetched = inp.fetch(target_dir, use_archive=True,
                                    decompress=True,
                                    disregard_copy_attribute=True)
            else:
                print('found {}'.format(inp.name))

def main():
    parser = build_parser()
    args = parser.parse_args()
    if args.package is None:
        args.package = GIOSYSTEM_ALGORITHMS.keys()
    print('packages to install: {}'.format(args.package))
    giosystemcore.settings.get_settings(args.settings_url)
    host = hostfactory.get_host()
    for pack in args.package:
        func = eval(GIOSYSTEM_ALGORITHMS[pack]['function'])
        func(host, args.bitbucket_user, args.bitbucket_password)

if __name__ == '__main__':
    main()
