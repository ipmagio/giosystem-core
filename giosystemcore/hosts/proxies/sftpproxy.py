"""
A module providing a class for executing file transfer operations through SFTP.
"""

import socket  # needed to check ftplib's error
socket.setdefaulttimeout(10)  # timeout for ftp connections, in seconds
import os
import re
import sys
from traceback import format_exception_only
import logging

import time
import pysftp

logger = logging.getLogger(__name__)

class SFTPClient_RETRY(object):

    def __init__(self, host, uname, pw, max_trys=10, sleep=1):
        self.logger = logger.getChild(self.__class__.__name__)
        self.connection_args = {'host':host,
                                'username':uname,
                                'password':pw}
        self.max_trys = max_trys
        self.sleep = sleep
        self.try_num = 0

    def get_connection(self):
        self.try_num += 1
        try:
            connection = pysftp.Connection(**self.connection_args)
        except pysftp.paramiko.SSHException as e:
            if self.try_num <= self.max_trys:
                self.logger.info(
                    "Reconnecting SFTP client...\n"
                    "Try number: {}/{}".format(self.try_num, self.max_trys))
                time.sleep(self.sleep)
                connection = self.get_connection()
            else:
                self.logger.error(e)
                connection = None
        return connection


class SftpProxyStateless(object):
    """
    Use SFTP to find and fetch files on a remote location

    This class does not cache the SFTP connection.
    """

    def __init__(self, local, remote):
        self.logger = logger.getChild(self.__class__.__name__)
        self.local_host = local
        self.remote_host = remote

    def find(self, path):
        self.logger.critical("using the stateless sftp to find: {}".format(path))
        found = []
        search_dir, search_pattern = os.path.split(path)
        patt_re = re.compile(search_pattern)
        try:
            sftp_retry = SFTPClient_RETRY(
                             self.remote_host.ip, 
                             self.remote_host.username,
                             self.remote_host.password
                         )
            sftp_retry.logger = self.logger.manager.getLogger(
                "{}:{}".format(self.logger.name, sftp_retry.logger.name)
            )
            with sftp_retry.get_connection() as sftp:
                sftp.chdir(search_dir)
                current_dir = sftp.getcwd()
                raw_file_list = sftp.listdir()
                for raw_path in raw_file_list:
                    if patt_re.search(raw_path) is not None:
                        found.append(os.path.join(current_dir, raw_path))
        except (IOError, pysftp.SSHException) as err:
            self.logger.critical(err)
        except Exception as err:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error_msg = "".join(format_exception_only(exc_type, exc_value))
            self.logger.critical("Unforeseen exception: {} {}".format(
                            err.__class__.__name__, error_msg))
            raise
        return found

    def fetch(self, path, destination):
        self.logger.critical("using the stateless sftp to fetch: {}".format(path))
        copied_path = None
        try:
            sftp_retry = SFTPClient_RETRY(
                             self.remote_host.ip, 
                             self.remote_host.username,
                             self.remote_host.password
                         )
            sftp_retry.logger = self.logger.manager.getLogger(
                "{}:{}".format(self.logger.name, sftp_retry.logger.name)
            )
            with sftp_retry.get_connection() as sftp:
                old_dir = os.getcwd()
                if not os.path.isdir(destination):
                    try:
                        os.makedirs(destination)
                    except OSError as e:
                        if e.args[0] == 17:
                            # the directory already exists, because some other
                            # concurrent process has created it
                            pass
                os.chdir(destination)
                dir_path, fname = os.path.split(path)
                sftp.get(path)
                copied_path = os.path.join(destination, fname)
                self.logger.critical("contents of destination dir: {}".format(
                                os.listdir(destination)))
                self.logger.critical("copied_path exists: {}".format(
                                os.path.isfile(copied_path)))
                os.chdir(old_dir)
        except (IOError, pysftp.SSHException) as err:
            self.logger.critical(err)
        except Exception as err:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error_msg = "".join(format_exception_only(exc_type, exc_value))
            self.logger.critical("Unforeseen exception: {} {}".format(
                            err.__class__.__name__, error_msg))
            raise
        return copied_path

    def send(self, path, destination):
        self.logger.warning("using the stateless sftp to send: {}".format(path))
        self.logger.debug('path: %s' % path)
        self.logger.debug('destination: %s' % destination)
        result = True
        try:
            sftp_retry = SFTPClient_RETRY(
                             self.remote_host.ip, 
                             self.remote_host.username,
                             self.remote_host.password
                         )
            sftp_retry.logger = self.logger.manager.getLogger(
                "{}:{}".format(self.logger.name, sftp_retry.logger.name)
            )
            with sftp_retry.get_connection() as sftp:
                directory, fname = os.path.split(path)
                dirs = self._create_remote_dirs(destination, sftp)
                if dirs:
                    ret_code = sftp.put(
                        path,
                        os.path.join(destination, fname)
                    )
                else:
                    raise
        except pysftp.SSHException as err:
            self.logger.error(err)
        except Exception as err:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error_msg = "".join(format_exception_only(exc_type, exc_value))
            self.logger.critical("Unforeseen exception: {} {}".format(
                            err.__class__.__name__, error_msg))
            raise
        return result

    def delete_file(self, path):
        """
        Delete the remote path.

        :arg path: The remote path to delete
        :type path: str
        """
        try:
            sftp_retry = SFTPClient_RETRY(
                             self.remote_host.ip, 
                             self.remote_host.username,
                             self.remote_host.password
                         )
            sftp_retry.logger = self.logger.manager.getLogger(
                "{}:{}".format(self.logger.name, sftp_retry.logger.name)
            )
            with sftp_retry.get_connection() as sftp:
                sftp.remove(path)
        except (IOError, pysftp.SSHException) as err:
            self.logger.debug(err)

    def run_command(self, command, local_bin, working_dir=None):
        result = None
        try:
            sftp_retry = SFTPClient_RETRY(
                             self.remote_host.ip, 
                             self.remote_host.username,
                             self.remote_host.password
                         )
            sftp_retry.logger = self.logger.manager.getLogger(
                "{}:{}".format(self.logger.name, sftp_retry.logger.name)
            )
            with sftp_retry.get_connection() as sftp:
                if working_dir is not None:
                    old_dir = sftp.execute("pwd")[0].strip()
                    if local_bin:
                        result = sftp.execute(
                            "cd {} && ./{}".format(working_dir, command))
                    else:
                        result = sftp.execute(
                            "cd {} && {}".format(working_dir, command))
                    sftp.chdir(old_dir)
                else:
                    result = sftp.execute(command)
        except pysftp.SSHException as err:
            self.logger.error(err)
        return result

    def close_connection(self):
        """
        This method is not used by this class. It is kept only for
        maintaining API compatibility.
        """
        self.logger.warning("using the stateless sftp in the close_connection "
                       "method")
        pass

    def _create_remote_dirs(self, path, connection):
        """
        Create the directory structure specified by 'path' on the remote host.
        """

        result = False
        out_list = connection.execute('mkdir -p %s' % path)
        if len(out_list) > 0:
            # Either the path already exists or it could not be created
            if 'exists' in out_list[0]:
                self.logger.info(out_list[0])
                result = True
            else:
                self.logger.error(out_list[0])  # something went wrong
        else:
            result = True
        return result

class SFTPProxy(object):
    """
    Connect to another server through SFTP and perform various actions.

    This class stores the SFTP connection in a local cache.
    """

    def __init__(self, local, remote):
        """
        Inputs:

            host - A G2Host object specifying the host that started this
                connection.
        """

        self.logger = logger.getChild(self.__class__.__name__)
        self.local_host = local
        self.remote_host = remote
        self.connection = None

    def _connect(self):
        result = True
        if self.connection is None:
            try:
                self.logger.debug('Connecting to %s...' % \
                                  self.remote_host.ip)

                sftp_retry = SFTPClient_RETRY(
                                 self.remote_host.ip, 
                                 self.remote_host.username,
                                 self.remote_host.password
                             )
                sftp_retry.logger = self.logger.manager.getLogger(
                    "{}:{}".format(self.logger.name, sftp_retry.logger.name)
                )
                self.connection = sftp_retry.get_connection()
                if self.connection is None:
                    result = False
            except pysftp.paramiko.AuthenticationException:
                self.connection = None
                result = False
        return result

    def find(self, path):
        """
        Return a list of paths that match the 'path' argument.

        Inputs:

            path - A string specifying a regular expression to be
                interpreted as the full directory plus a pattern for
                the file's name
        """

        found = []
        if self._connect():
            search_dir, search_pattern = os.path.split(path)
            patt_re = re.compile(search_pattern)
            try:
                self.connection.chdir(search_dir)
                current_dir = self.connection.getcwd()
                raw_file_list = self.connection.listdir()
                for raw_path in raw_file_list:
                    if patt_re.search(raw_path) is not None:
                        found.append(os.path.join(current_dir,
                                     raw_path))
            except IOError as err:
                self.logger.debug(err)
        else:
            self.logger.error('Not connected to the remote SFTP host')
        return found

    def run_command(self, command, local_bin, working_dir=None):
        result = None
        if self._connect():
            if working_dir is not None:
                old_dir = self.connection.execute('pwd')[0].strip()
                if local_bin:
                    result = self.connection.execute('cd %s && ./%s' % \
                                                     (working_dir, command))
                else:
                    result = self.connection.execute('cd %s && %s' % \
                                                     (working_dir, command))
                self.connection.chdir(old_dir)
            else:
                result = self.connection.execute(command)
        return result

    def delete_file(self, path):
        """
        Delete the remote path.

        :arg path: The remote path to delete
        :type path: str
        """

        if self._connect():
            try:
                self.connection.remove(path)
            except IOError as err:
                self.logger.debug(err)
        else:
            self.logger.error('Not connected to the remote SFTP host')

    def fetch(self, path, destination):
        """
        Fetch the input paths from remoteHost.

        The paths are copied to localHost's destination.

        Inputs:

            paths - A string with the full path of the file to get.

            destination - The directory on this instance's localHost attribute
                where the file is to be copied to.

        Returns:

            The full path to the newly fetched file.
        """

        copied_path = None
        if self._connect():
            old_dir = os.getcwd()
            if not os.path.isdir(destination):
                try:
                    os.makedirs(destination)
                except OSError as e:
                    if e.args[0] == 17:
                        # the directory already exists, because some other
                        # concurrent process has created it
                        pass
            os.chdir(destination)
            dir_path, fname = os.path.split(path)
            self.connection.get(path)
            copied_path = os.path.join(destination, fname)
            os.chdir(old_dir)
        else:
            self.logger.error("Not connected to the remote SFTP host")
        return copied_path

    def send(self, path, destination):
        """
        Put the local path to the remote server.

        Inputs:

            path - A string with the path in the local file system. It is
                assumed to contain the full path to the file and not a search
                pattern.

            destination - The directory on the remote server where the
                path will be put. It will be created in case it doesn't
                exist.

        Returns:

            A boolean specifying the overall result of the operation.
        """

        self.logger.debug('path: %s' % path)
        self.logger.debug('destination: %s' % destination)
        result = True
        if self._connect():
            directory, fname = os.path.split(path)
            dirs = self._create_remote_dirs(destination)
            if dirs:
                ret_code = self.connection.put(
                    path,
                    os.path.join(destination, fname)
                )
            else:
                raise
        else:
            self.logger.error('Not connected to the remote SFTP host')
            result = False
        return result

    def _create_remote_dirs(self, path):
        """
        Create the directory structure specified by 'path' on the remote host.
        """

        result = False
        if self._connect:
            out_list = self.connection.execute('mkdir -p %s' % path)
            if len(out_list) > 0:
                # Either the path already exists or it could not be created
                if 'exists' in out_list[0]:
                    self.logger.info(out_list[0])
                    result = True
                else:
                    # something went wrong
                    self.logger.error(out_list[0])
            else:
                result = True
        else:
            self.logger.error('Not connected to the remote SFTP host')
        return result

    def close_connection(self):
        if self.connection is not None:
            self.connection.close()
            self.connection = None



