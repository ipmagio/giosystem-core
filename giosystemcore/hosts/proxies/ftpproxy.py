from ftplib import FTP, error_temp, error_perm
import socket # needed to check ftplib's error
socket.setdefaulttimeout(10) # timeout for ftp connections, in seconds
import os
import re
import logging
import time

logger = logging.getLogger(__name__)

class FTPProxy(object):
    '''
    Connect to another server through FTP and perform various actions.

    Note: This proxy will try to connect with the remote host using
          two methods. One uses the host IP address and the other
          uses the hostname. The second one takes place if the given
          IP address is '0.0.0.0' or in case of the first method, by
          IP, fails.
          The hostname is build by joining the coresetting Host
          attributes 'Name' and 'Domain' with a dot.

          Example:
                    Name: xpto
                    Domain: meteo.pt

                    hostname: xpto.meteo.pt
              
    '''

    def __init__(self, local, remote):
        '''
        Inputs:

            host - A G2Host object specifying the host that started this 
                connection. 
        '''

        self.logger = logger.getChild(self.__class__.__name__)
        self.connection = FTP()
        self.local_host = local
        self.remote_host = remote

    def _test_for_connection(self):
        '''Return True if the connection has already been established.'''

        return self.connection.getwelcome() is not None

    def _connect(self, timeout_retries=3):
        '''
        Establish a new connection to the remote server.

        Inputs:

            timeout_retries - An integer specifying the number of seconds
                to wait before reconnecting after a timeout error.

        Returns:

            A boolean with the result of the connection attempt.
        '''

        result = False
        if self._test_for_connection():
            #self.logger.debug('The connection already exists')
            try:
                # check for timeouts
                self.connection.pwd()
                result = True
            except error_temp, errMsg:
                patt = re.compile(r'(^\d+)')
                errNum = patt.search(str(errMsg)).group(1)
                if errNum == '421':
                    # timeout, try to connect again in timeout_retries seconds
                    self.logger.info('The FTP connection timed out. Waiting '\
                                     '%i seconds' % timeout_retries)
                    time.sleep(timeout_retries)
                    del self.connection
                    self.connection = FTP()
                    result = self._connect()
        else:
            result = self.create_connection()
        return result

    def create_connection(self):
        result = False
        self.logger.debug('Connecting to %s...' % self.remote_host.name)
        try:
            if self.remote_host.ip == '0.0.0.0':
                raise socket.error
            self.connection.connect(self.remote_host.ip)
            self.connection.login(self.remote_host.username, 
                                  self.remote_host.password)
            result = True
            self.logger.debug('Connection successful')
        except socket.error, errorMsg:
            try:
                self.connection.connect('.'.join([self.remote_host.name,
                                                  self.remote_host.domain]))
                self.connection.login(self.remote_host.username, 
                                      self.remote_host.password)
                result = True
                self.logger.debug('Connection successful')
            except socket.error, errorMsg:
                if errorMsg[0] == 113:
                    self.logger.error('%s unreachable. Maybe the host is ' +\
                                        'down?' % self.remote_host.host)
                else:
                    self.logger.warning('socket error: %s' % (str(errorMsg)))
        except error_perm, errMsg:
            patt = re.compile(r'(^\d+)')
            err_num = patt.search(str(errMsg)).group(1)
            if err_num == '530':
                self.logger.error(errMsg)
                self.connection.quit()
                self.connection = None
                self.connection = FTP()
        return result

    def find(self, path):
        '''
        Return a list of paths that match the input path's pattern.

        Inputs:

            path - A string specifying a regular expression to be
                interpreted as the full directory plus a pattern for
                the file's name
        '''

        found = []
        if self._connect():
            search_dir, search_pattern = os.path.split(path)
            self.logger.debug('search_dir: %s' % search_dir)
            self.logger.debug('search_pattern: %s' % search_pattern)
            patt_re = re.compile(search_pattern)
            try:
                self.connection.cwd(search_dir)
                current_dir = self.connection.pwd()
                raw_file_list = self.connection.nlst()
                for raw_path in raw_file_list:
                    if patt_re.search(raw_path) is not None:
                        found.append(os.path.join(current_dir, raw_path))
            except error_perm, msg:
                self.logger.warning(msg)
                #raise
        return found

    # FIXME
    # - test this method out
    def fetch(self, path, destination):
        '''
        Fetch the input path from remoteHost.

        The path are copied to localHost's destination.

        Inputs:

            path - A string with the full path of the file to get. This string
                is assumed to contain a real path and not a regular expression.

            destination - The directory on this instance's localHost attribute
                where the file is to be copied to.

        Returns:

            The full path to the newly fetched file.
        '''

        copied_path = None
        if self._connect():
            old_dir = os.getcwd()
            if not os.path.isdir(destination):
                os.makedirs(destination)
            os.chdir(destination)
            dir_path, fname = os.path.split(path)
            fh = open(fname, 'wb')
            self.connection.retrbinary('RETR %s' % path, 
                                       fh.write)
            fh.close()
            copied_path = os.path.join(destination, fname)
            os.chdir(old_dir)
        return copied_path

    def send(self, path, destination, temporary_extension='.tmp'):
        '''
        Put the local path to the remote server.

        Inputs:

            path - A string with the path in the local file system. It is
                assumed to contain a full path not search pattern.

            destination - The directory on the remote server where the
                path will be put. It will be created in case it doesn't 
                exist.

            temporary_extension - The extension to use while copying the
                file. After copying the file is renamed back to its
                original name.

        Returns:

            A boolean indicating if the transfer was successful.
        '''

        result = True
        if self._connect():
            old_dir = os.getcwd()
            dir_path, fname = os.path.split(path)
            temporary_name = ''.join((fname, temporary_extension))
            self.logger.debug('path: %s' % path)
            self.logger.debug('fname: %s' % fname)
            self.logger.debug('temporary_name: %s' % temporary_name)
            if os.path.isdir(dir_path):
                os.chdir(dir_path)
                self._create_remote_dirs(destination)
                self.connection.cwd(destination)
                result = self.connection.storbinary("STOR %s" % \
                                                    temporary_name,
                                                    open(fname, "rb"))
                result_code = result.split()[0]
                if result_code == '226':
                    self.rename(temporary_name, fname)
                    self.connection.rename(temporary_name, fname)
                else:
                    result = False
            os.chdir(old_dir)
        return result

    def _create_remote_dirs(self, path):
        '''
        Create the directory structure specified by 'path' on the remote host.
        '''

        old_dir = self.connection.pwd()
        self.connection.cwd("/")
        for part in path.split("/"):
            try:
                self.connection.cwd(part)
            except error_perm:
                self.connection.mkd(part)
                self.connection.cwd(part)
        self.connection.cwd(old_dir)

    def delete_file(self, path):
        """
        Delete the remote path.

        :arg path: The remote path to delete
        :type path: str
        """
        if self._connect():
            self.logger.debug('remove: %s' % path)
            self.connection.delete(path)
