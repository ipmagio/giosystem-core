"""
"""

import re
import logging


from lxml import etree
import requests
from enum import Enum
import owslib.csw as csw

logger = logging.getLogger(__name__)


class CatalogueType(Enum):
    GEONETWORK = 1
    PYCSW = 2


def csw_authenticate(some_function):
    """A decorator to perform authentication on CswInterface methods

    Use it on a derived class of `BaseCswInterface`. Be sure to reimplement
    the `login` and `logout` methods too.
    """

    def wrapper(*args, **kwargs):
        instance = args[0]
        instance.logout()
        instance.login()
        result = some_function(*args, **kwargs)
        instance.logout()
        return result
    return wrapper


def get_catalogue(catalogue_type, catalogue_endpoint, user=None,
                  password=None):
    """A factory for giosystemcatalogues"""
    catalogue_class = {
        CatalogueType.GEONETWORK: CswInterfaceGeonetwork,
        CatalogueType.PYCSW: CswInterfacePycsw,
    }.get(catalogue_type)
    return catalogue_class(catalogue_endpoint, user=user, password=password)


class BaseCswInterface(object):

    ENCODING = 'utf-8'
    CSW_VERSION = '2.0.2'
    SERVICE_NAME = 'CSW'

    nsmap = {
        'csw': 'http://www.opengis.net/cat/csw/2.0.2',
        'ogc': 'http://www.opengis.net/ogc',
        'dc': 'http://purl.org/dc/elements/1.1/',
        'ows': 'http://www.opengis.net/ows',
        'gco': 'http://www.isotc211.org/2005/gco',
        'gmd': 'http://www.isotc211.org/2005/gmd',
    }
    request_headers = {
        'Content-Type': 'application/xml',
        'Accept': 'application/xml',
    }

    def __init__(self, catalogue_endpoint, user=None, password=None,
                 search_suffix=None, transaction_suffix=None):
        if search_suffix is not None:
            self.search_endpoint = "/".join((catalogue_endpoint,
                                             search_suffix))
        else:
            self.search_endpoint = catalogue_endpoint
        if transaction_suffix is not None:
            self.transaction_endpoint = "/".join((catalogue_endpoint,
                                                  transaction_suffix))
        else:
            self.transaction_endpoint = catalogue_endpoint
        self.user = user
        self.password = password
        self._session = requests.Session()

    def get_capabilities(self):
        params = {
            "service": self.SERVICE_NAME,
            "version": self.CSW_VERSION,
            "request": "GetCapabilities",
        }
        response = self._session.get(self.search_endpoint, params=params,
                                     headers=self.request_headers)
        return response

    # TODO - Improve this method
    def get_single_record(self, search_pattern):
        """
        Return the XML record from the database for a GioFile

        This method is not using owslib because there is some error with it
        """

        msg = """<?xml version="1.0"?>
          <csw:GetRecords xmlns:csw="http://www.opengis.net/cat/csw/2.0.2" service="CSW" version="2.0.2" resultType="results" ElementSetName="full" outputSchema="http://www.isotc211.org/2005/gmd">
            <csw:Query typeNames="gmd:MD_Metadata">
              <csw:Constraint version="1.1.0">
                <csw:CqlText>Title like '{}'</csw:CqlText>
              </csw:Constraint>
            </csw:Query>
           </csw:GetRecords>"""

        response = requests.post(self.search_endpoint,
                                 headers=self.request_headers,
                                 data=msg.format(search_pattern))
        result = ""
        if response.status_code == 200:
            result = response.text
        return result

    def find_records_by_pattern(self, title_pattern):
        '''
        Return the fileIdentifier of a record

        This method will issue a GetRecords request with a CQL filter
        looking for a Title element that matches the input pattern.

        This method is not using owslib because there is some error with it
        '''

        msg = '''<?xml version="1.0"?>
          <csw:GetRecords xmlns:csw="http://www.opengis.net/cat/csw/2.0.2" service="CSW" version="2.0.2" resultType="results" ElementSetName="full" outputSchema="http://www.isotc211.org/2005/gmd">
            <csw:Query typeNames="gmd:MD_Metadata">
              <csw:Constraint version="1.1.0">
                <csw:CqlText>Title like '{}'</csw:CqlText>
              </csw:Constraint>
            </csw:Query>
           </csw:GetRecords>'''

        response = requests.post(self.search_endpoint,
                                 headers=self.request_headers,
                                 data=msg.format(title_pattern))
        identifiers = []
        if response.status_code == 200:
            tree = etree.fromstring(response.text.encode("utf-8"))
            identifiers = tree.xpath("//gmd:fileIdentifier/gco:"
                                     "CharacterString/text()",
                                     namespaces=self.nsmap)
        return identifiers

    def get_records(self, ids):
        """
        Perform a GetRecordById request and return its response.

        Parameters
        ----------

        ids: iterable
            The ids of the records to retrieve from the catalogue

        Returns
        -------
        list
            A list of tuples with the id and title of each of the found
            records.

        """

        response = self._session.get(
            self.search_endpoint,
            params={
                "service": self.SERVICE_NAME,
                "version": self.CSW_VERSION,
                "request": "GetRecordById",
                "id": ",".join(ids),
            }
        )
        found_ids = []
        if response.status_code == 200:
            response_element = etree.fromstring(
                response.text.encode(self.ENCODING))
            found_ids = response_element.xpath(
                "csw:SummaryRecord/dc:identifier/text()",
                namespaces=self.nsmap
            )
        return found_ids

    # UNTESTED
    def get_records_by_product(self, product, timeslot):
        '''
        '''

        cat = csw.CatalogueServiceWeb(self.search_endpoint)
        cat.getrecords(cql='csw:Title like "%{}%{}%"'.format(
            product, timeslot))
        return cat.records

    def insert_records(self, records):
        """Insert input records into CSW server."""
        content_elements = []
        for record in records:
            try:
                # a schema validation would also be in order here
                record = etree.fromstring(record.encode(self.ENCODING))
                insert_el = etree.Element(
                    '{{{csw}}}Insert'.format(**self.nsmap))
                insert_el.append(record)
                content_elements.append(insert_el)
            except IOError as err:
                print(err)
        response = self._perform_transaction(content_elements)
        try:
            inserted_records = response.xpath(
                'csw:TransactionSummary/csw:totalInserted/text()',
                namespaces=self.nsmap
            )[0]
        except IndexError:
            inserted_records = 0
        num_inserted = int(inserted_records)
        if num_inserted == len(records):
            result = True
            details = 'Inserted {} records in the catalogue'.format(
                num_inserted)
        else:
            result = False
            response_details = self._get_exception_details(response)
            if num_inserted == 0:
                details = ("Could not insert any of the {} records in "
                           "the catalogue. {}".format(len(records),
                                                      response_details))
            else:
                details = ("Could only insert {} of the {} records in the "
                           "catalogue. {}".format(num_inserted, len(records),
                                                  response_details))
        return result, details

    def delete_records(self, ids):
        """Delete records from the CSW catalogue."""
        content_elements = []
        for id in ids:
            delete_element = etree.Element(
                "{{{csw}}}Delete".format(**self.nsmap),
                nsmap=self.nsmap
            )
            constraint_element = etree.SubElement(
                delete_element, "{{{csw}}}Constraint".format(**self.nsmap),
                attrib={"version": "1.1.0"}, nsmap=self.nsmap
            )
            filter_element = etree.SubElement(
                constraint_element,
                "{{{ogc}}}Filter".format(**self.nsmap), nsmap=self.nsmap
            )
            prop_is_equal_to_element = etree.SubElement(
                filter_element,
                "{{{ogc}}}PropertyIsEqualTo".format(**self.nsmap),
                nsmap=self.nsmap
            )
            prop_name_element = etree.SubElement(
                prop_is_equal_to_element,
                "{{{ogc}}}PropertyName".format(**self.nsmap),
                nsmap=self.nsmap
            )
            prop_name_element.text = "apiso:Identifier"
            literal_element = etree.SubElement(
                prop_is_equal_to_element,
                "{{{ogc}}}Literal".format(**self.nsmap),
                nsmap=self.nsmap
            )
            literal_element.text = id
            content_elements.append(delete_element)
        response = self._perform_transaction(content_elements)
        num_deleted = int(response.xpath(
            "csw:TransactionSummary/csw:totalDeleted/text()",
            namespaces=self.nsmap
        )[0])
        if num_deleted == len(ids):
            result = True
            details = "Deleted {} records from the catalogue".format(
                num_deleted)
        else:
            result = False
            if num_deleted == 0:
                details = "Could not delete any of the requested record ids."
            else:
                details = ("Could only delete {} of the {} requested "
                           "record ids".format(num_deleted, len(ids)))
        return result, details

    def replace_record(self, record):
        details = ""
        result = True
        try:
            record_element = etree.fromstring(record.encode(self.ENCODING))
            identifier = record_element.xpath(
                "gmd:fileIdentifier/gco:CharacterString/text()",
                namespaces=self.nsmap
            )[0]
        except IndexError:
            details += "Could not extract record identifier."
        except etree.XMLSyntaxError:
            details += "Could not parse record into XML"
        else:
            found = self.get_records([identifier])
            delete_result = False
            if len(found) == 1:
                delete_result, delete_details = self.delete_records(
                    [identifier])
                details += delete_details
            if (len(found) == 1 and delete_result) or not any(found):
                insert_result, insert_details = self.insert_records(
                    [record])
                details += insert_details
                result = result and result
        return result, details

    def login(self):
        """
        To be reimplemented in base classes, if needed
        """
        pass

    def logout(self):
        """
        To be reimplemented in base classes, if needed
        """
        pass

    def _get_exception_details(self, response_element):
        details = response_element.xpath(
            "ows:Exception/ows:ExceptionText/text()",
            namespaces=self.nsmap
        )
        return details

    def _parse_response(self, response):
        """
        :arg response:
        :type response: requests.response
        """

        encoding_pattern = r' encoding="(?P<encoding>[\w-]*)"'
        encoding_obj = re.search(encoding_pattern, response.text)
        encoding = None
        if encoding_obj is not None:
            encoding = encoding_obj.groupdict()['encoding']
        stripped_text = re.sub(encoding_pattern, '', response.text)
        tree = etree.fromstring(stripped_text)
        return encoding, tree

    def _perform_transaction(self, content_elements):
        root_element = etree.Element(
            "{{{}}}Transaction".format(self.nsmap["csw"]),
            attrib={"version": self.CSW_VERSION, "service": self.SERVICE_NAME},
            nsmap=self.nsmap
        )
        for element in content_elements:
            root_element.append(element)
        tree = etree.ElementTree(root_element)
        transaction_data = etree.tostring(tree, encoding=self.ENCODING,
                                          xml_declaration=True,
                                          pretty_print=True)
        response = self._session.post(self.transaction_endpoint,
                                      data=transaction_data,
                                      headers=self.request_headers)
        encoding, response_tree = self._parse_response(response)
        return response_tree



class CswInterfacePycsw(BaseCswInterface):
    """
    This class is used to interface with pycsw
    """
    pass


class CswInterfaceGeonetwork(BaseCswInterface):
    """
    This class is used to interface with geonetwork
    """

    _auth_suffixes  = {
        "login": "j_spring_security_check",
        "logout": "j_spring_security_logout",
    }

    def __init__(self, catalogue_endpoint, user=None, password=None):
        super(CswInterfaceGeonetwork, self).__init__(
            catalogue_endpoint, user=user, password=password,
            search_suffix="srv/eng/csw",
            transaction_suffix="srv/eng/csw-publication"
        )
        self.login_url = "/".join(
            (catalogue_endpoint, self._auth_suffixes["login"]))
        self.logout_url = "/".join(
            (catalogue_endpoint, self._auth_suffixes["logout"]))

    @csw_authenticate
    def insert_records(self, records):
        """Perform a CSW transaction for inserting records.

        :arg records: The xml metadata files to insert into the catalogue
        :type records: list
        """

        return super(CswInterfaceGeonetwork, self).insert_records(records)

    @csw_authenticate
    def delete_records(self, record_ids):
        """Perform a CSW transaction for deleting records.

        :arg record_ids: The identifiers of the records to be deleted
        :type records: list
        """

        return super(CswInterfaceGeonetwork, self).delete_records(record_ids)

    def login(self):
        login_data = {
            "username": self.user,
            "password": self.password,
        }
        response = self._session.post(self.login_url, data=login_data)
        return response

    def logout(self):
        return self._session.post(self.logout_url)

