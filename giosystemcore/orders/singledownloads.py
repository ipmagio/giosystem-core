'''
'''

import datetime as dt
import os
from uuid import uuid1
import zipfile

import giosystemcore.hosts.hostfactory as hf
import giosystemcore.catalogue.cswinterface as cswinterface


class DownloadPreparator(object):

    def __init__(self, product_name, timeslot, tile='', use_io_buffer=True,
                 use_archive=True):
        '''

        :arg product_name:
        :type product_name: str

        :arg timeslot:
        :type timeslot: str

        :arg tile:
        :type tile: str
        '''

        self._bundle = dict()
        self.use_io_buffer = use_io_buffer
        self.use_archive = use_archive
        self.tile = tile
        self.product = product_name
        self.host = hf.get_host()
        self.working_dir = os.path.join(self.host.temp_dir, 
                                        'giosystemdownloads',
                                        str(uuid1()))
        self.timeslot = dt.datetime.strptime(timeslot, '%Y%m%d%H%M')
        self.output_base_dir = os.path.join(self.host.data_dirs[0],
                                            'DOWNLOADABLE_DATA',
                                            self.product,
                                            '{}'.format(self.timeslot.year),
                                            '{:02d}'.format(self.timeslot.month))

    def get_zip(self):
        '''
        Return or create a new zip file with the product, quicklook and xml
        '''

        bundle = self._get_bundle()
        zip_name = '.'.join((bundle['product'].search_pattern, 'zip'))
        found_zips = self.host.find(os.path.join(self.output_base_dir, zip_name))
        if any(found_zips):
            the_zip = found_zips.pop()
        else:
            the_zip = self._build_zip(zip_name)
        return the_zip

    def get_product(self):
        '''
        '''

        bundle = self._get_bundle()
        raise NotImplementedError

    def get_quicklook(self, force_new=False):
        '''
        Fetch the quicklook from the local filesystem or force generation
        '''

        bundle = self._get_bundle()
        ql = bundle.get('quicklook')
        sent = None
        if ql is not None:
            sent = bundle['quicklook'].fetch(
                self.output_base_dir,
                use_io_buffer=self.use_io_buffer,
                use_archive=self.use_archive,
                decompress=True
            )
        if force_new:
            print('generating a new quicklook...')
        return sent

    def get_user_manual(self):
        '''
        '''

        raise NotImplementedError

    # Test this out
    # the xml string must not include the getrecords result output
    def get_metadata(self, output_dir=None):
        '''
        '''

        if output_dir is None:
            output_dir = self.output_base_dir
        bundle = self._get_bundle()
        gio_file = bundle['product']
        c = cswinterface.CswInterface()
        xml = c.get_single_record(gio_file)
        if xml != '':
            if not self.host.dir_exists(self.output_base_dir):
                self.host.create_directory_tree(output_dir)
            output_path = '{}.xml'.format(os.path.join(output_dir,
                                          gio_file.search_pattern))
            with open(output_path, 'w') as fh:
                fh.write(xml.encode('utf8'))
            result = output_path
        else:
            result = None
        return result

    def _get_bundle(self, force=False):
        '''
        '''

        if not any(self._bundle) or force:
            settings_manager = giosystemcore.settings.get_settings()
            fs = settings_manager.get_all_files_settings()
            file_names = [n for n, s in fs.iteritems() if self.tile in s['search_pattern']]
            bundle = {}
            for name in file_names:
                gio_file = giosystemcore.files.get_file(name, self.timeslot)
                if gio_file.product is not None and \
                        gio_file.product.short_name == self.product:
                    file_type = gio_file.file_type
                    if file_type == 'hdf5':
                        bundle['product'] = gio_file
                    elif file_type == 'png':
                        bundle['quicklook'] = gio_file
                    # we do not need the metadata giofile because we use the
                    # 'product' to extract the metadata from the catalogue
                    #elif file_type == 'xml':
                    #    bundle['metadata'] = gio_file
            self._bundle = bundle
        return self._bundle

    def _build_zip(self, zip_name):
        bundle = self._get_bundle()
        fetched_dir = os.path.join(self.working_dir, 'fetched')
        if not self.host.dir_exists(fetched_dir):
            self.host.create_directory_tree(fetched_dir)
        temp_path = os.path.join(self.working_dir, zip_name)
        with zipfile.ZipFile(temp_path, 'w') as fh:
            for role, gio_file in bundle.iteritems():
                fetched = gio_file.fetch(fetched_dir, 
                                         use_io_buffer=self.use_io_buffer,
                                         use_archive=self.use_archive,
                                         decompress=False)
                if fetched is not None:
                    fh.write(fetched, os.path.basename(fetched))
        result, sent_path = self.host.send(temp_path, self.output_base_dir)
        self.host.remove_directory(self.working_dir)
        return sent_path
