#-*-coding: utf-8-*-
from __future__ import absolute_import
import datetime as dt
import logging
import os
import re

import pystache

from .processingalgorithms import ExternalAlgorithm
from ...utilities import create_directory_tree

logger = logging.getLogger(__name__)


class ProcessGlobalLstNewResolution(ExternalAlgorithm):

    def __init__(self, *args, **kwargs):
        super(ProcessGlobalLstNewResolution, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def execute(self, fetched):
        self._get_external_program()
        available_files = self._filter_inputs(
            [p for p in fetched.values() if p is not None])
        available_files.sort()
        pcf_path = self._write_product_configuration_file(available_files)
        acf_path = self._write_algorithm_configuration_file()
        out, result = self._run_external_algorithm(pcf_path, acf_path)
        details = ''
        if not result:
            details = out
        return result, details

    def _filter_inputs(self, input_paths):
        lst_paths = [f for f in input_paths if '_LST_' in f]
        src_patt = re.compile('(.*HDF5_LSASAF_MSG_LST_([^_]*)_.*|'
                              '.*HDF5_([^_]*)_GEOLAND2_LST_.*)')
        filtered_paths = [path for path in input_paths if 'LWMASK' in path]
        for lst_path in lst_paths:
            mres = src_patt.match(lst_path)
            if mres is not None:
                source = mres.group(2) if mres.group(3) is None else mres.group(3)
                for path in input_paths:
                    if source in os.path.basename(path):
                        filtered_paths.append(path)
        return filtered_paths

    def _write_algorithm_configuration_file(self):
        encoding = "utf-8"
        template_renderer = pystache.Renderer(
            file_encoding=encoding,
            search_dirs=os.path.join(os.path.dirname(__file__),
                                     "acf_templates")
        )
        template = template_renderer.load_template("lst")
        context = self._generate_acf_context()
        rendered = template_renderer.render(template, context)
        acf_path = os.path.join(self.working_dir,
                                "algorithm_configuration_file.txt")
        create_directory_tree(self.working_dir)
        with open(acf_path, "w") as fh:
            fh.write(rendered.encode(encoding))
        return acf_path

    def _generate_acf_context(self):
        date_now = dt.datetime.utcnow()
        time_coverage_start = self.timeslot - dt.timedelta(minutes=30)
        time_coverage_end = self.timeslot + dt.timedelta(minutes=30)
        iso_tz_format = "%Y-%m-%dT%H:%M:%SZ"
        context = {
            "timeslot_iso": self.timeslot.strftime("%Y-%m-%d %H:%M:%S"),
            "timeslot_iso_tz": self.timeslot.strftime(iso_tz_format),
            "creation_date_iso_tz": date_now.strftime(iso_tz_format),
            "time_coverage_start_iso_tz": time_coverage_start.strftime(
                iso_tz_format),
            "time_coverage_end_iso_tz": time_coverage_end.strftime(
                iso_tz_format),
            "europe_lst_version": "7.14.0",
            "europe_satellite_name": "MSG3",
            "europe_satellite_sensor": "SEVI",
            "america_lst_version": "3.40",
            "america_satellite_name": "GOES13",
            "america_satellite_sensor": "IMAG",
            "asia_lst_version": "3.50",
            "asia_satellite_name": "HIMAWARI8",
            "asia_satellite_sensor": "AHI",
            "ngp2grid_version": "5.2",
            "product_identifier": ("urn:cgls:global:lst_v1_0.045degree:LST_"
                                   "{timeslot:%Y%m%d%H%M}_GLOBE_GEO_"
                                   "V1.2".format(timeslot=self.timeslot)),
            "parent_identifier": "urn:cgls:global:lst_v1_0.045degree",
        }
        return context

    def _write_product_configuration_file(self, input_paths):
        """Write the product configuration file.

        This method is reimplemented from its base class because the 
        NGP2GRID_g2 algortihm requires a special syntax for defining the 
        output files.

        Inputs:

            input_paths - a list of strings with the paths of the inputs
                to the package

        Returns:

            The full path to the newly-written product configuration file.
        """

        pcf_path = os.path.join(self.working_dir,
                                'product_configuration_file.txt')
        yfileinp = 'yfileinp = {}'.format(
            repr(input_paths)[1:-1].replace(",", ",\n").replace("u'", "'"))

        out_fnames = [f.search_pattern for f in self.outputs]
        output_paths = [os.path.join(self.working_dir, out_name) for
                        out_name in out_fnames]
        yfileout = 'yfileout = {}'.format(
            repr(output_paths)[1:-1].replace(",", ",\n").replace("u'", "'"))
        contents = [
            '&NAM_ALG_MODES\n',
            'MODE = 1\n',
            '/\n',
            '&NAM_ALG_INPUT_FILES_PATH\n',
            '%s\n' % yfileinp,
            '/\n',
            '&NAM_ALG_OUTPUT_FILES_PATH\n',
            '%s\n' % yfileout,
            '/\n',
            '&NAM_STOP_STATUS_RETRIEVAL_FREQ\n',
            'yfreqinsecs = 1\n',
            '/\n'
        ]
        path = self.host.create_file(pcf_path, contents)
        return path
