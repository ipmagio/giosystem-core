"""
This module holds classes that perform the pre-processing of the input
LRIT satellite data.
"""

from __future__ import absolute_import
import os
import logging

from .processingalgorithms import ExternalAlgorithm

logger = logging.getLogger(__name__)

class ProcessRef(ExternalAlgorithm):
    """
    This class should not be instantiated directly. Use the appropriate
    child class instead.
    """

    K = None
    CWL = None  # Central wavelength
    BINARY_NAME = 'ref'
    COMPRESSION_LEVEL = 9

    def __init__(self, *args, **kwargs):
        super(ProcessRef, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def _write_algorithm_configuration_file(self):
        """ For the Radiances to Reflectances conversion process,
            source dependent values of k and respective central
            wavelengths (cwl) must be given.

                              k = pi / H

            (Where H is the "solar incident irradiance" or
            "solar spectral irradiance")

            Values for each source can be found at:
            GOES -> http://www.ospo.noaa.gov/Operations/GOES/calibration/goes-vis-ch-calibration.html
            HIMAWARI/MTSAT2 -> http://ds.data.jma.go.jp/mscweb/data/monitoring/gsics/vis/techinfo_visvical.html
        """
        acf_path = os.path.join(self.working_dir,
                                'algorithm_configuration_file.txt')
        contents = [
            '&NAM_AlgorithmConfigNamelist\n',
            'AreaName = "{}",\n'.format(self.AREA),
            'k = {},\n'.format(self.K),
            'cwl = {},\n'.format(self.CWL),
            'compression_level = {}\n'.format(
                self.parameters.get('compression_level',
                                    self.COMPRESSION_LEVEL)),
            '/\n',
        ]
        path = self.host.create_file(acf_path, contents)
        return path


class ProcessRefGoes(ProcessRef):

    AREA = 'GOES-Disk'
    K = '1.89544E-3'
    CWL = '0.65'


class ProcessRefMtsat(ProcessRef):

    AREA = 'MTSAT-Disk'
    K = '2.096855E-3'
    CWL = '0.675'

