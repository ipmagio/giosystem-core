"""
This module holds classes that calculate the historical stats used as 
input to the cloud mask algorithm.
"""

from __future__ import absolute_import
import os
import logging

from .processingalgorithms import ExternalAlgorithm

logger = logging.getLogger(__name__)

class ProcessCloudMask(ExternalAlgorithm):
    """
    This class should not be instantiated directly. Use the appropriate
    child class instead.
    """

    # compression levels:
    # * 0 - uncompressed
    # * 1 - lowest compression rate
    # * 9 - highest compression rate (smaller files)
    COMPRESSION_LEVEL = 9

    def __init__(self, *args, **kwargs):
        super(ProcessCloudMask, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def _write_algorithm_configuration_file(self):
        acf_path = os.path.join(self.working_dir,
                                'algorithm_configuration_file.txt')
        contents = [
            '&NAM_AREAS\n',
            'AreaName = \'%s\'\n' % self.AREA,
            '/\n',
            '&NAM_SAT\n',
            'SatName = \'%s\'\n' % self.AREA.replace('-Disk', ''),
            '/\n',
            '&NAM_REF_THRESHOLD\n',
            'experience = {}\n'.format(
                int(self.parameters.get('experience', '3'))
            ),
            'r006Threshold = {:0.2f}\n'.format(
                float(self.parameters.get('r006Threshold', '0.05'))),
            '/\n',
            '&TESTS_FLAGS\n',
            'faz_lstt = {}\n'.format(self.parameters.get('faz_lstt', '.T.')),
            'faz_sf = {}\n'.format(self.parameters.get('faz_sf', '.T.')),
            '/\n',
            '&NAM_OUTPUT_COMPRESSION\n',
            'compression_level = {}\n'.format(self.COMPRESSION_LEVEL),
            '/\n',
        ]
        path = self.host.create_file(acf_path, contents)
        return path


class ProcessCloudMaskGoes(ProcessCloudMask):

    AREA = 'GOES-Disk'


class ProcessCloudMaskMtsat(ProcessCloudMask):

    AREA = 'MTSAT-Disk'


class ProcessCloudMaskHimawari(ProcessCloudMask):

    AREA = 'HMWR-Disk'
