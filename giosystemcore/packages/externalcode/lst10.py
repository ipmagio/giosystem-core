#-*- encoding: utf-8-*-
"""
This module holds classes that calculate the historical stats used as 
input to the lst10s_soam algorithm.
"""

from __future__ import absolute_import
import datetime as dt
import logging
import os
import re

from netcdf_cglops_tools import netcdfanalyzer

from ...utilities import write_namelists
from .processingalgorithms import ExternalAlgorithm

logger = logging.getLogger(__name__)


def get_platforms_and_sensors(input_paths):
    """Extract satellite and sensor information from LST products.

    Parameters
    ----------
    input_paths: list
        Paths to the NetCDF products to analyze

    Returns
    -------
    list
        A sequence of two-element tuples with satellite names and the
        respective sensors

    """

    source_re = re.compile(r".*/g2_BIOPAR_LST_.*_GLOBE_GEO.*")
    platforms = []
    sensors = []
    for path in input_paths:
        match_result = source_re.match(path)
        if match_result is not None:
            info = netcdfanalyzer.ProductInfo(path)
            platforms += [plat for plat in info.platform if
                          plat not in platforms]
            sensors += [sensor for sensor in info.sensor if
                        sensor not in sensors]
    result = zip(platforms, sensors)
    return result


class ProcessLst10(ExternalAlgorithm):
    """This class should not be instantiated directly. Use the appropriate
    child class instead.

    """

    BINARY_NAME = 'lst10_g2'

    # compression levels:
    # * 0 - uncompressed
    # * 1 - lowest compression rate
    # * 9 - highest compression rate (smaller files)
    COMPRESSION_LEVEL = 9

    def __init__(self, *args, **kwargs):
        super(ProcessLst10, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def run_pre_execution(self):
        """Make sure this package does not compress its outputs.

        If the underlying algorithm generates outputs that use internal
        compression there should be no need to further compress them.

        """

        super(ProcessLst10, self).run_pre_execution()
        if self.COMPRESSION_LEVEL > 0:
            self.compress_outputs = False

    def execute(self, fetched):
        self._get_external_program()
        available_files = [p for p in fetched.values() if p is not None]
        available_files.sort()
        pcf_path = self._write_product_configuration_file(available_files)
        acf_path = self._write_algorithm_configuration_file(available_files)
        out, result = self._run_external_algorithm(pcf_path, acf_path)
        details = ''
        if not result:
            details = out
        return result, details

    def archive(self):
        """Make sure this package does not compress its outputs.

        Since the underlying algorithm is creating HDF5 outputs with
        internal compression enabled, it is not necessary to further compress
        the outputs of this package when sending them to the archive.

        """

        if self.COMPRESSION_LEVEL > 0:
            self.compress_outputs = False
        super(ProcessLst10, self).archive()

    def _write_product_configuration_file(self, input_paths):
        """Write the product configuration file.
        
        Parameters
        ----------
        input_paths: list
            Paths of the inputs to the package

        Returns
        -------
        str
            Full path to the newly-written product configuration file.

        """
        
        pcf_path = os.path.join(self.working_dir,
                                'product_configuration_file.txt')
        yfileinp = 'yfileinp='
        input_paths.sort()
        for input_path in input_paths:
            yfileinp += '\'%s\',\n' % input_path
        yfileinp = yfileinp[:-2]  # removing the last newline and comma
        output_paths = []
        for out in self.outputs:
            out_path = os.path.join(self.working_dir,
                                    out.search_pattern.replace("(|.bz2)$", ""))
            output_paths.append(out_path)
        output_paths.sort()
        yfileout = 'yfileout='
        for output_path in output_paths:
            yfileout += '\'%s\',\n' % output_path
        yfileout = yfileout[:-2]  # removing the last newline and comma
        contents = [
            '&NAM_ALG_MODES\n',
            'MODE = 1\n',
            '/\n',
            '&NAM_ALG_INPUT_FILES_PATH\n',
            '%s\n' % yfileinp,
            '/\n',
            '&NAM_ALG_OUTPUT_FILES_PATH\n',
            '%s\n' % yfileout,
            '/\n',
            '&NAM_STOP_STATUS_RETRIEVAL_FREQ\n',
            'yfreqinsecs = 1\n',
            '/\n',
        ]
        path = self.host.create_file(pcf_path, contents)
        return path

    def _get_general_namelists(self, plat_info, execution_mode,
                               title_pattern, comment, name, long_name,
                               identifier_pattern, product_version,
                               algorithm_version, parent_identifier):

        date_now = dt.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        time_coverage_start = sorted(
            [gio_file.timeslot for gio_file in self.inputs if
             gio_file.frequency == 'dynamic']
        )[0]
        time_coverage_end = sorted(
            [gio_file.timeslot for gio_file in self.inputs if
             gio_file.frequency == 'dynamic']
        )[-1]
        namelists = [
            {
                "name": "PRODUCT",
                "default_values": {
                    "mode": execution_mode,
                }
            },
            {
                "name": "OUTPUT_COMPRESSION",
                "default_values": {
                    "compression_level": self.COMPRESSION_LEVEL,
                }
            },
            {
                "name": "TIME_AXIS",
                "default_values": {
                    "time_unit": "days since {0:%Y-%m-%d %H:00:00}".format(
                        time_coverage_start),
                    "time_value": 0.0,
                }
            },
            {
                "name": "THREADS",
                "default_values": {
                    "n_threads": 5,
                    "max_size_in_Mbytes": 20,
                }
            },
            {
                "name": "ROOT_STR_ATTRS",
                "default_values": {
                    "Conventions": "CF-1.6",
                    "title": title_pattern.format(
                        timeslot=time_coverage_start),
                    "institution": "IPMA",
                    "archive_facility": "IPMA",
                    "source": 'Data was derived from satellite imagery.',
                    "history": '{} - Product generation;'.format(date_now),
                    "references": (
                        'Product User Manual: http://land.copernicus.eu/'
                        'global/sites/default/files/products/\n'
                        'Validation Report: http://land.copernicus.eu/global/'
                        'sites/default/files/products/\n'
                        'Product page: http://land.copernicus.eu/global/'
                        'products/lst'
                    ),
                    "comment": comment,
                    "name": name,
                    "long_name": long_name,
                    "identifier": identifier_pattern.format(
                        timeslot=time_coverage_start),
                    "date_created": date_now,
                    "product_version": product_version,
                    "algorithm_version": algorithm_version,
                    "time_coverage_start": time_coverage_start.strftime(
                        "%Y-%m-%dT00:00:00Z"),
                    "time_coverage_end": time_coverage_end.strftime(
                        "%Y-%m-%dT23:00:00Z"),
                    "platform": ", ".join(
                        [plat_attr[0] for plat_attr in plat_info]),
                    "sensor": ", ".join(
                        [plat_attr[1] for plat_attr in plat_info]),
                    "orbit_type": 'GEO',
                    "processing_level": 'L4',
                    "processing_mode": 'Near Real Time',
                    "contacts": (
                        'Principal investigator (Researcher): '
                        'isabel.trigo@ipma.pt; Instituto Português do Mar e '
                        'da Atmosfera (IPMA); Rua C ao Aeroporto; Lisbon; '
                        '1749-077; Portugal (PT); IPMA website; '
                        'http://www.ipma.pt\n'
                        'Originator (IPMA GIO-Global Land Help Desk): '
                        'sandra.coelho@ipma.pt; Instituto Português do Mar e '
                        'da Atmosfera (IPMA); Rua C ao Aeroporto; Lisbon; '
                        '1749-077; Portugal (PT); IPMA website; '
                        'http://www.ipma.pt\n'
                        'Point of contact (GIO-Global Land Help Desk): '
                        'helpdesk@vgt.vito.be; Flemish Institute for '
                        'Technological Research (VITO); Boeretang 200; Mol; '
                        '2400; Belgium (BE); VITO website; '
                        'http://land.copernicus.eu/global/\n'
                        'Owner: ENTR-COPERNICUS-ASSETS@ec.europa.eu; '
                        'European Commission Directorate-General for '
                        'Enterprise and Industry (EC-DGEI); Avenue '
                        'd\'Auderghem 45; Brussels; 1049; Belgium (BE); '
                        'EC-DGEI website; http://ec.europa.eu/enterprise/\n'
                        'Custodian (Responsible): copernicuslandproducts@jrc.'
                        'ec.europa.eu; European Commission Directorate-'
                        'General Joint Research Center (JRC); Via E.Fermi, '
                        '249; Ispra; 21027; Italy (IT); JRC website; '
                        'http://ies.jrc.ec.europa.eu'
                    ),
                    "parent_identifier": parent_identifier,
                    "inspire_theme": 'Orthoimagery',
                    "gemet_keywords": 'solar radiation',
                    "gcmd_keywords": 'SURFACE TEMPERATURE',
                    "other_keywords": 'Global',
                    "iso19115_topic_categories": (
                        'climatologyMeteorologyAtmosphere, '
                        'imageryBaseMapsEarthCover'
                    ),
                    "credit": (
                        'LST10 products are generated by the land service of '
                        'Copernicus, the Earth Observation programme of '
                        'the European Commission. The LST algorithm, '
                        'originally developed in the framework of the '
                        'FP7/Geoland2, is generated from MTSAT/HIMAWARI and '
                        'GOES data, respectively owned by JMA and NOAA, and '
                        'combined with the LST product from MSG under '
                        'copyright EUMETSAT, produced by LSA-SAF.'
                    ),
                    "purpose": (
                        'This product is first designed to fit the '
                        'requirements of the Global Land component of Land '
                        'Service of GMES-Copernicus. It can be also useful '
                        'for all applications related to the environment '
                        'monitoring.'
                    ),
                }
            },
        ]
        return namelists

    def _get_variable_namelists(self):
        return [
            {
                "name": "MAX_BASE_ATTRS",
                "default_values": {
                    'max_FillValue': -8000,
                    'max_valid_range': '-7000,8000',
                    'max_scale_factor': 0.01,
                    'max_add_offset': 273.15,
                },
            },
            {
                "name": "MAX_STR_ATTRS",
                "default_values": {
                    'long_name': 'LST Maximum',
                    'units': 'Kelvin',
                    'cell_methods': 'area:mean where land',
                    'ancillary_variables': 'FRAC_VALID_OBS',
                    'coverage_content_type': 'physicalMeasurement',
                    'comment': ('Maximum LST values observed during the '
                                'compositing period; one layer per hour.'),
                    'grid_mapping': 'crs',
                },
            },
            {
                "name": "MIN_BASE_ATTRS",
                "default_values": {
                    'min_FillValue': -8000,
                    'min_valid_range': '-7000,8000',
                    'min_scale_factor': 0.01,
                    'min_add_offset': 273.15,
                },
            },
            {
                "name": "MIN_STR_ATTRS",
                "default_values": {
                    'long_name': 'LST Minimum',
                    'units': 'Kelvin',
                    'cell_methods': 'area:mean where land',
                    'ancillary_variables': 'FRAC_VALID_OBS',
                    'coverage_content_type': 'physicalMeasurement',
                    'comment': ('Minimum LST values observed during the '
                                'compositing period; one layer per hour.'),
                    'grid_mapping': 'crs',
                },
            },
            {
                "name": "MEDIAN_BASE_ATTRS",
                "default_values": {
                    'median_FillValue': -8000,
                    'median_valid_range': '-7000,8000',
                    'median_scale_factor': 0.01,
                    'median_add_offset': 273.15,
                },
            },
            {
                "name": "MEDIAN_STR_ATTRS",
                "default_values": {
                    'long_name': 'LST Median',
                    'units': 'Kelvin',
                    'cell_methods': 'area:mean where land',
                    'ancillary_variables': 'FRAC_VALID_OBS',
                    'coverage_content_type': 'physicalMeasurement',
                    'comment': ('Median LST values observed during the '
                                'compositing period; one layer per hour.'),
                    'grid_mapping': 'crs',
                },
            },
            {
                "name": "FRAC_VALID_OBS_BASE_ATTRS",
                "default_values": {
                    'frac_valid_obs_FillValue': -8000,
                    'frac_valid_obs_valid_range': '0,100',
                    'frac_valid_obs_scale_factor': 0.01,
                },
            },
            {
                "name": "FRAC_VALID_OBS_STR_ATTRS",
                "default_values": {
                    'long_name': 'Fraction of Valid Observations',
                    'units': 'Kelvin',
                    'cell_methods': 'area:mean where land',
                    'comment': ('Fraction of valid observations as used to '
                                'calculate the statistical parameters; one '
                                'layer per hour.'),
                    'grid_mapping': 'crs',
                },
            },
        ]

    @staticmethod
    def _get_grid_mapping_namelists():
        return [
            {
                "name": "GRIDMAPPING_DOUBLE_ATTRS",
                "default_values": {
                    'inverse_flattening': 298.257223563,
                    'longitude_of_prime_meridian': 0.0,
                    'semi_major_axis': 6378137.0,
                },
            },
            {
                "name": "GRIDMAPPING_STR_ATTRS",
                "default_values": {
                    'grid_mapping_name': 'latitude_longitude',
                },
            }
        ]

    def _prepare_namelists(self, plat_info):
        title_pattern = ("Global 10-day Daily Cycle Land Surface "
                         "Temperature for {timeslot:%Y-%m-%d}")
        identifier_pattern = ('urn:cgls:global:lst10_v1_0.045degree:LST10_'
                              '{timeslot:%Y%m%d}_GLOBE_GEO_V1.0')
        general_namelists = self._get_general_namelists(
            plat_info,
            execution_mode="daily_cycle",
            title_pattern=title_pattern,
            comment=(
                '10-day Daily Cycle Land Surface Temperature (LST10) '
                'provides a statistical overview of the LST daily '
                'cycle over each 10-day compositing for every image '
                'pixel. LST10 is useful for the scientific '
                'community, namely for those dealing with '
                'meteorological and climate models. Accurate values '
                'of LST are also of special interest in a wide range '
                'of areas related to land surface processes, '
                'including meteorology, hydrology, agrometeorology, '
                'climatology and environmental studies.'
            ),
            name="LST10",
            long_name='10-day Daily Cycle Land Surface Temperature',
            identifier_pattern=identifier_pattern,
            product_version='V1.0',
            algorithm_version='LST10_v2.0',
            parent_identifier='urn:cgls:global:lst10_v1_0.045degree',
        )
        variable_namelists = self._get_variable_namelists()
        grid_mapping_namelists = self._get_grid_mapping_namelists()
        namelists = (general_namelists + variable_namelists +
                     grid_mapping_namelists)
        return namelists

    def _write_algorithm_configuration_file(self, input_paths):
        """Write a namelist file with settings for algorithm configuration."""

        acf_path = os.path.join(self.working_dir,
                                'algorithm_configuration_file.txt')
        plat_info = get_platforms_and_sensors(input_paths)
        namelists = self._prepare_namelists(plat_info)
        contents = write_namelists(namelists, self.parameters)
        path = self.host.create_file(acf_path, contents)
        return path



class ProcessLst10S(ProcessLst10):
    """This class should not be instantiated directly. Use the appropriate
    child class instead.

    """

    def _prepare_namelists(self, plat_info):
        collection_identifier = 'urn:cgls:global:lst10s_v1_0.045degree'
        title_pattern = ("Global 10-day Synthesis Land Surface "
                         "Temperature for {timeslot:%Y-%m-%d}")
        identifier_pattern = (
            collection_identifier + ':LST10S_{timeslot:%Y%m%d}_GLOBE_GEO_V1.0')
        general_namelists = self._get_general_namelists(
            plat_info,
            execution_mode="tci",
            title_pattern=title_pattern,
            comment=(
                '10-day Synthesis Land Surface Temperature (LST10S) provides '
                'a statistical overview of the LST over each 10-day '
                'compositing for every image pixel. LST10S is useful for the '
                'scientific community, namely for those dealing with '
                'meteorological and climate models. Accurate values of LST '
                'are also of special interest in a wide range of areas '
                'related to land surface processes, including meteorology, '
                'hydrology, agrometeorology, climatology and environmental '
                'studies.'
            ),
            name="LST10S",
            long_name='10-day Synthesis Land Surface Temperature',
            identifier_pattern=identifier_pattern,
            product_version='V1.0',
            algorithm_version='LST10_v2.0',
            parent_identifier=collection_identifier,
        )
        variable_namelists = self._get_variable_namelists()
        grid_mapping_namelists = self._get_grid_mapping_namelists()
        namelists = (general_namelists + variable_namelists +
                     grid_mapping_namelists)
        return namelists

    def _get_variable_namelists(self):
        return [
            {
                "name": "MAX_BASE_ATTRS",
                "default_values": {
                    'max_FillValue': -8000,
                    'max_valid_range': '-7000,8000',
                    'max_scale_factor': 0.01,
                    'max_add_offset': 273.15,
                },
            },
            {
                "name": "MAX_STR_ATTRS",
                "default_values": {
                    'long_name': 'LST Maximum',
                    'units': 'Kelvin',
                    'cell_methods': 'area:mean where land',
                    'ancillary_variables': 'FRAC_VALID_OBS',
                    'coverage_content_type': 'physicalMeasurement',
                    'comment': ('Maximum LST value observed during the '
                                'compositing period regardless the time of '
                                'the day.'),
                    'grid_mapping': 'crs',
                },
            },
            {
                "name": "MIN_BASE_ATTRS",
                "default_values": {
                    'min_FillValue': -8000,
                    'min_valid_range': '-7000,8000',
                    'min_scale_factor': 0.01,
                    'min_add_offset': 273.15,
                },
            },
            {
                "name": "MIN_STR_ATTRS",
                "default_values": {
                    'long_name': 'LST Minimum',
                    'units': 'Kelvin',
                    'cell_methods': 'area:mean where land',
                    'ancillary_variables': 'FRAC_VALID_OBS',
                    'coverage_content_type': 'physicalMeasurement',
                    'comment': ('Minimum LST values observed during the '
                                'compositing period regardless the time of '
                                'the day.'),
                    'grid_mapping': 'crs',
                },
            },
            {
                "name": "MEDIAN_BASE_ATTRS",
                "default_values": {
                    'median_FillValue': -8000,
                    'median_valid_range': '-7000,8000',
                    'median_scale_factor': 0.01,
                    'median_add_offset': 273.15,
                },
            },
            {
                "name": "MEDIAN_STR_ATTRS",
                "default_values": {
                    'long_name': 'LST Median',
                    'units': 'Kelvin',
                    'cell_methods': 'area:mean where land',
                    'ancillary_variables': 'FRAC_VALID_OBS',
                    'coverage_content_type': 'physicalMeasurement',
                    'comment': ('Median LST values observed during the '
                                'compositing period regardless the time of '
                                'the day.'),
                    'grid_mapping': 'crs',
                },
            },
            {
                "name": "FRAC_VALID_OBS_BASE_ATTRS",
                "default_values": {
                    'frac_valid_obs_FillValue': -8000,
                    'frac_valid_obs_valid_range': '0,100',
                    'frac_valid_obs_scale_factor': 0.01,
                },
            },
            {
                "name": "FRAC_VALID_OBS_STR_ATTRS",
                "default_values": {
                    'long_name': 'Fraction of Valid Observations',
                    'units': 'Kelvin',
                    'cell_methods': 'area:mean where land',
                    'comment': ('Fraction of valid observations as used to '
                                'calculate the statistical parameters '
                                'regardless the time of the day'),
                    'grid_mapping': 'crs',
                },
            },
            {
                "name": 'TCI_BASE_ATTRS',
                "default_values": {
                    'tci_FillValue': -8000,
                    'tci_valid_range': '0,10000',
                    'tci_scale_factor': 0.0001,
                },
            },
            {
                "name": 'TCI_STR_ATTRS',
                "default_values": {
                    'long_name': 'Thermal Conditions Index',
                    'cell_methods': 'area:mean',
                    'coverage_content_type': 'physicalMeasurement',
                    'comment': (
                        'Thermal Conditions Index for the compositing period, '
                        'estimated using LST observations around local solar '
                        'noon.'
                    ),
                    'grid_mapping': 'crs',
                },
            }
        ]


class MergeDailyCycle(ProcessLst10):
    """
    This class should not be instantiated directly. Use the appropriate
    child class instead.
    """

    def _write_algorithm_configuration_file(self, input_paths):
        """
        Write a namelist file with the required
        settings for the algorithm configuration.
        """

        return ""


class ProcessLstCl(ProcessLst10):
    """
    This class should not be instantiated directly. Use the appropriate
    child class instead.
    """

    def _write_algorithm_configuration_file(self):
        """
        Write a namelist file with the required
        settings for the algorithm configuration.
        """

        acf_path = os.path.join(self.working_dir,
                                'algorithm_configuration_file.txt')
        contents = [

            '&NAM_PRODUCT\n',
            'mode = "climate"\n',
            '/\n',
            
            '&NAM_GENERAL_STR_ATTRS\n',
            'CENTRE                    = "IPMA"\n',
            'ARCHIVE_FACILITY          = "IPMA"\n',
            'PRODUCT                   = "LST_CL"\n',
            'PARENT_PRODUCT_NAME       = "LST"\n',
            'PRODUCT_ALGORITHM_VERSION = "1.0"\n',
            'ORBIT_TYPE                = "GEO"\n',
            'TIME_RANGE                = "one year"\n',
            'Conventions               = "CF-1.5"\n',
            'PROJECTION_NAME           = "Plate Carree"\n',
            '/\n',
            
            '&NAM_GENERAL_INT_ATTRS\n',
            'COMPRESSION    = 0\n',
            '/\n',
            
            '&NAM_DATASET_STR_ATTRS\n',
            'PRODUCT        = "MAX_MAX"            , "MIN_MAX"            , "Q_FLAG"               , "MAX_MAX_UPDATE_TIME", "MIN_MAX_UPDATE_TIME"\n',
            'long_name      = "maximum of maximums", "minimum of maximums", "climate quality flags", "max_max update time", "min_max update time"\n',
            'units          = "Celsius"            , "Celsius"            , "Dimensionless"        , "days"               , "days"\n',
            'coordinates    = "lat lon"            , "lat lon"            , "lat lon"              , "lat lon"            , "lat lon"\n',
            'grid_mapping   = "equirectangular"    , "equirectangular"    , "equirectangular"      , "equirectangular"    , "equirectangular"\n',
            '/\n',

            '&NAM_DATASET_INT_ATTRS\n',
            'PRODUCT_ID     =  255 ,  255 ,  255 ,  255 , 255\n',
            '_FillValue     = -8000, -8000, -8000, -8000, -8000\n',
            'NB_BYTES       =  2   ,  2   ,  2   ,  2   ,  2\n',
            '/\n',
            
            '&NAM_DATASET_FLOAT_ATTRS\n',
            'scale_factor = 0.01, 0.01, 1.0, 0.01, 0.01\n',
            'add_offset   = 0.0 , 0.0 , 0.0, 0.0 , 0.0\n',
            '/\n',

            '&NAM_PROJECTION_STR_ATTRS\n',
            'grid_mapping_name = equirectangular\n',
            'spatial_ref = "PROJCS[\\\"WGS 84 / Plate Carree (deprecated)\\\",GEOGCS[\\\"WGS 84\\\",DATUM[\\\"WGS_1984\\\",SPHEROID[\\\"WGS 84\\\",6378137,298.257223563,AUTHORITY[\\\"EPSG\\\",\\\"7030\\\"]],AUTHORITY[\\\"EPSG\\\",\\\"6326\\\"]],PRIMEM[\\\"Greenwich\\\",0,AUTHORITY[\\\"EPSG\\\",\\\"8901\\\"]],UNIT[\\\"degree\\\",0.0174532925199433,AUTHORITY[\\\"EPSG\\\",\\\"9122\\\"]],AUTHORITY[\\\"EPSG\\\",\\\"4326\\\"]],PROJECTION[\\\"Equirectangular\\\"],PARAMETER[\\\"latitude_of_origin\\\",0],PARAMETER[\\\"central_meridian\\\",0],PARAMETER[\\\"false_easting\\\",0],PARAMETER[\\\"false_northing\\\",0],UNIT[\\\"metre\\\",1,AUTHORITY[\\\"EPSG\\\",\\\"9001\\\"]],AXIS[\\\"X\\\",EAST],AXIS[\\\"Y\\\",NORTH],AUTHORITY[\\\"EPSG\\\",\\\"32662\\\"]]"\n',
            '/\n',

            '&NAM_PROJECTION_DOUBLE_ATTRS\n',
            'false_easting = 0.0\n',
            'false_northing = 0.0\n',
            'inverse_flattening = 298.257223563\n',
            'latitude_of_projection_origin = 0.0\n',
            'longitude_of_central_meridian = 0.0\n',
            'longitude_of_prime_meridian = 0.0\n',
            'semi_major_axis = 6378137.0\n',
            '/\n',

            '&NAM_THREADS\n',
            'n_threads = 5\n',
            'max_size_in_Mbytes = 100\n',
            '/\n',

        ]

        path = self.host.create_file(acf_path, contents)
        return path
