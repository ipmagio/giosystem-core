"""
This module has special classes that take care of altering a GIOFile's
timeslot and search strings as well as some functions for creating
multiple rules.
"""

import re
import datetime as dt
import calendar

from .. import utilities


def create_decadal_interval_input_rules(base_timeslot, package_offsets,
                                        fix_hour=False):
    reference_timeslot = utilities.offset_timeslot(
        base_timeslot,
        package_offsets['years'],
        package_offsets['months'],
        package_offsets['days'],
        package_offsets['hours'],
        package_offsets['minutes'],
        offset_decades=package_offsets['decades']
    )
    day = reference_timeslot.day
    if day in range(1, 11):  # decade 1
        start_offset_days = 1 - day
        end_offset_days = 10 - day
    elif day in range(11, 21):  # decade 2
        start_offset_days = 11 - day
        end_offset_days = 20 - day
    else:  # decade 3
        start_offset_days = 21 - day
        last_day_of_month = calendar.monthrange(reference_timeslot.year,
                                                reference_timeslot.month)[1]
        end_offset_days = last_day_of_month - day
    if fix_hour:
        frequency_offsets = {'days': 1, }
        start_offset_hours = 0
        end_offset_hours = 0
    else:
        frequency_offsets = {'hours': 1}
        start_offset_hours = -reference_timeslot.hour
        end_offset_hours = 23 - reference_timeslot.hour
    start_offsets = {'days': start_offset_days,
                     'hours': start_offset_hours}
    end_offsets = {'days': end_offset_days,
                   'hours': end_offset_hours}
    rules = create_arbitrary_interval_input_rules(base_timeslot,
                                                  package_offsets,
                                                  frequency_offsets,
                                                  start_offsets,
                                                  end_offsets)
    return rules


def create_arbitrary_interval_input_rules(base_timeslot, package_offsets,
                                          frequency_offsets, start_offsets,
                                          end_offsets):
    """
    Create the SelectionRule objects necessary for inputs of type 'multiple'.

    This function only creates objects of type OffsetRule.

    Inputs:

        base_timeslot - the timeslot to be used as a base for the OffsetRule
            objects that will be created

        specifications - a dictionary with the interval specifications. It
            must have the following key/value pairs:

                - frequency_offsets: a dictionary with the offsets to apply
                    when iterating through each step of the interval
                - start_offsets: a dictionary with the offsets to apply
                    to the base_timeslot when determining the start of the
                    interval
                - end_offsets: a dictionary with the offsets to apply
                    to the base_timeslot when determining the end of the
                    interval

            Each of the previous dictionaries must have the following
            key/value pairs:

                - years
                - months
                - days
                - hours
                - minutes
    """

    if base_timeslot is None:
        base_timeslot = dt.datetime.utcnow()
    reference_timeslot = utilities.offset_timeslot(
        base_timeslot,
        package_offsets['years'],
        package_offsets['months'],
        package_offsets['days'],
        package_offsets['hours'],
        package_offsets['minutes'],
        offset_decades=package_offsets['decades']
    )
    start = utilities.offset_timeslot(
        reference_timeslot,
        start_offsets.get('years', 0),
        start_offsets.get('months', 0),
        start_offsets.get('days', 0),
        start_offsets.get('hours', 0),
        start_offsets.get('minutes', 0)
    )
    end = utilities.offset_timeslot(
        reference_timeslot,
        end_offsets.get('years', 0),
        end_offsets.get('months', 0),
        end_offsets.get('days', 0),
        end_offsets.get('hours', 0),
        end_offsets.get('minutes', 0)
    )
    rules = []
    current = start
    index = 0
    while current <= end:
        all_year_offset = start_offsets.get('years', 0) + \
            frequency_offsets.get('years', 0) * index
        all_month_offset = start_offsets.get('months', 0) + \
            frequency_offsets.get('months', 0) * index
        all_day_offset = start_offsets.get('days', 0) + \
            frequency_offsets.get('days', 0) * index
        all_hour_offset = start_offsets.get('hours', 0) + \
            frequency_offsets.get('hours', 0) * index
        all_minute_offset = start_offsets.get('minutes', 0) + \
            frequency_offsets.get('minutes', 0) * index
        rule = OffsetRule(
            base_timeslot, 
            offset_years=package_offsets['years'],
            offset_months=package_offsets['months'],
            offset_days=package_offsets['days'],
            offset_hours=package_offsets['hours'],
            offset_minutes=package_offsets['minutes'],
            offset_decades=package_offsets['decades'],
            specific_offset_years=all_year_offset,
            specific_offset_months=all_month_offset,
            specific_offset_days=all_day_offset,
            specific_offset_hours=all_hour_offset,
            specific_offset_minutes=all_minute_offset
        )
        rules.append(rule)
        current = utilities.offset_timeslot(current,
                                            frequency_offsets.get('years', 0),
                                            frequency_offsets.get('months', 0),
                                            frequency_offsets.get('days', 0),
                                            frequency_offsets.get('hours', 0),
                                            frequency_offsets.get('minutes', 0))
        index += 1
    return rules


class SelectionRule(object):
    """
    Base class for all selection rules. It is not to be instantiated directly.
    """

    @property
    def base_timeslot(self):
        return self._base_timeslot

    @base_timeslot.setter
    def base_timeslot(self, ts):
        if isinstance(ts, basestring):
            self._base_timeslot = dt.datetime.strptime(ts, '%Y%m%d%H%M')
        else:
            self._base_timeslot = ts

    @property
    def reference_timeslot(self):
        if self.base_timeslot is not None:
            ts = utilities.offset_timeslot(self.base_timeslot,
                                           self.offset_years,
                                           self.offset_months,
                                           self.offset_days,
                                           self.offset_hours,
                                           self.offset_minutes)
        else:
            ts = None
        return ts

    @property
    def timeslot(self):
        return None

    @property
    def year(self):
        return None

    @property
    def month(self):
        return None

    @property
    def day(self):
        return None

    @property
    def hour(self):
        return None

    @property
    def minute(self):
        return None

    def __init__(self, base_timeslot=None, offset_years=0, offset_months=0,
                 offset_days=0, offset_hours=0, offset_minutes=0,
                 offset_decades=0):
        """
        Inputs:

            base_timeslot - either a datetime object with the base timeslot to
                consider or a string to convert to a proper datetime. The
                string must have 'YYYYMMDDhhmm' format.

            offset_years - an integer specifying how many years to offset
                the base timeslot

            offset_months - an integer specifying how many months to offset
                the base timeslot

            offset_days - an integer specifying how many days to offset
                the base timeslot

            offset_hours - an integer specifying how many hours to offset
                the base timeslot

            offset_minutes - an integer specifying how many minutes to offset
                the base timeslot

            offset_decades - an integer specifying how many day decades to 
                offset the base timeslot
        """

        self.base_timeslot = base_timeslot
        self.offset_years = offset_years
        self.offset_months = offset_months
        self.offset_days = offset_days
        self.offset_hours = offset_hours
        self.offset_minutes = offset_minutes
        self.offset_decades = offset_decades

    def __repr__(self):
        offsets = []
        if self.offset_years != 0:
            offsets.append('years: %r' % self.offset_years)
        if self.offset_months != 0:
            offsets.append('months: %r' % self.offset_months)
        if self.offset_days != 0:
            offsets.append('days: %r' % self.offset_days)
        if self.offset_hours != 0:
            offsets.append('hours: %r' % self.offset_hours)
        if self.offset_minutes != 0:
            offsets.append('minutes: %r' % self.offset_minutes)
        if self.offset_decades != 0:
            offsets.append('decades: %r' % self.offset_decades)
        return '%r(base_timeslot=%r, offsets={%r})' % \
            (self.__class__.__name__, self.base_timeslot, ', '.join(offsets))


class OffsetRule(SelectionRule):

    @property
    def timeslot(self):
        timeslot = utilities.offset_timeslot(
            self.reference_timeslot,
            self.specific_offset_years,
            self.specific_offset_months,
            self.specific_offset_days,
            self.specific_offset_hours,
            self.specific_offset_minutes,
            offset_decades=self.offset_decades
        )
        return timeslot

    @property
    def year(self):
        return self.timeslot.strftime('%Y')

    @property
    def month(self):
        return self.timeslot.strftime('%m')

    @property
    def day(self):
        return self.timeslot.strftime('%d')

    @property
    def julian_day(self):
        return self.timeslot.strftime('%j')

    @property
    def hour(self):
        return self.timeslot.strftime('%H')

    @property
    def minute(self):
        return self.timeslot.strftime('%M')

    def __init__(self, base_timeslot, offset_years=0, offset_months=0,
                 offset_days=0, offset_hours=0, offset_minutes=0,
                 offset_decades=0, specific_offset_years=0,
                 specific_offset_months=0, specific_offset_days=0, 
                 specific_offset_hours=0, specific_offset_minutes=0,
                 specific_offset_decades=0):
        super(OffsetRule, self).__init__(base_timeslot, offset_years,
                                         offset_months, offset_days,
                                         offset_hours, offset_minutes,
                                         offset_decades)
        self.specific_offset_years = specific_offset_years
        self.specific_offset_months = specific_offset_months
        self.specific_offset_days = specific_offset_days
        self.specific_offset_hours = specific_offset_hours
        self.specific_offset_minutes = specific_offset_minutes
        self.specific_offset_decades = specific_offset_decades

    def filter_paths(self, paths):
        """
        Return the first path on the list of input paths, after sorting.

        The list of input paths is first sorted alphabetically and in
        increasing order and then the first path on the list is chosen.
        """

        sorted_paths = sorted(paths)
        return sorted_paths[0]


class AgeRule(SelectionRule):

    YOUNGEST = 'youngest'
    OLDEST = 'oldest'
    BEFORE = 'before'
    AFTER = 'after'
    IGNORE = 'ignore'

    @property
    def timeslot(self):
        return None

    @property
    def year(self):
        if self.fix_year:
            result = self.reference_timeslot.strftime('%Y')
        else:
            result = '\d{4}'
        return result

    @property
    def month(self):
        if self.fix_month:
            result = self.reference_timeslot.strftime('%m')
        else:
            result = '\d{2}'
        return result

    @property
    def day(self):
        if self.fix_day:
            result = self.reference_timeslot.strftime('%d')
        else:
            result = '\d{2}'
        return result

    @property
    def hour(self):
        if self.fix_hour:
            result = self.reference_timeslot.strftime('%H')
        else:
            result = '\d{2}'
        return result

    @property
    def minute(self):
        return '\d{2}'

    def __init__(self, base_timeslot, offset_years=0, offset_months=0,
                 offset_days=0, offset_hours=0, offset_minutes=0,
                 offset_decades=0, age=YOUNGEST, 
                 relative_to_reference=BEFORE, fix_year=False, fix_month=False,
                 fix_day=False, fix_hour=False):
        super(AgeRule, self).__init__(base_timeslot, offset_years,
                                      offset_months, offset_days,
                                      offset_hours, offset_minutes,
                                      offset_decades)
        if age not in (self.YOUNGEST, self.OLDEST):
            raise ValueError('Invalid age parameter')
        if relative_to_reference not in (self.BEFORE, self.AFTER, self.IGNORE):
            raise ValueError('Invalid relative_to_reference parameter')
        self.base_timeslot = base_timeslot
        self.age = age
        self.relative_to_reference = relative_to_reference
        self.fix_hour = fix_hour
        self.fix_day = fix_day
        self.fix_month = fix_month
        self.fix_year = fix_year

    def filter_paths(self, paths):
        eligible_paths = []
        timeslot_string = ''.join((self.year, self.month, self.day,
                                  self.hour, self.minute))
        filter_ = re.compile(timeslot_string)
        for path in paths:
            if filter_.search(path) is not None:
                ts = utilities.extract_timeslot_from_path(path)
                if ts is not None:
                    relative_map = {
                        self.BEFORE: ts < self.reference_timeslot,
                        self.AFTER: ts > self.reference_timeslot,
                        self.IGNORE: True,
                    }
                    if relative_map[self.relative_to_reference]:
                        eligible_paths.append(path)
        result = None
        if any(eligible_paths):
            eligible_paths.sort(key=utilities.extract_timeslot_from_path)
            age_map = {self.OLDEST: 0, self.YOUNGEST: -1}
            result = eligible_paths[age_map[self.age]]
        return result
