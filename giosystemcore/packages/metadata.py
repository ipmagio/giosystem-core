"""
This module holds classes for creating the XML metadata records and uploading
them to the CSW catalogue.
"""

from __future__ import absolute_import
from collections import namedtuple
import os
import logging

from ..catalogue import metadata
from ..catalogue import cswinterface
from ..hosts.hostfactory import get_web_server
from .basepackage import GioPackage


logger = logging.getLogger(__name__)


CatalogueDetails = namedtuple(
    "CatalogueDetails",
    "type_ endpoint username password"
)
CatalogueDetails.__new__.__defaults__ = (None, None)


class MetadataCreator(GioPackage):
    """Generate XML metadata records and upload them to CSW catalogue(s)

    This class will inspect its inputs and build metadata records from their
    attributes. These metadata records are then submitted to the configured
    CSW catalogues.

    Since the generated metadata records are uploaded to the catalogue, it
    makes no sense for packages created from this class to define any outputs.
    """

    def __init__(self, *args, **kwargs):
        super(MetadataCreator, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def execute(self, fetched=None):
        fetched = fetched or dict()
        gen = metadata.NetCdfMetadataGenerator()
        all_result = True
        all_details = ""
        for record in (gen.generate_metadata(path) for path in
                       fetched.itervalues()):
            for cat in self.get_catalogues():
                result, details = cat.replace_record(record)
                all_result = all_result and result
                all_details = " ".join((all_details, details))
        return all_result, all_details

    # TODO: this method should be part of package's initialization
    # however, this refactoring is not straightforward because currently
    # giosystem basepackage does not load parameters in __init__()
    def get_catalogues(self):
        """Return interfaces for catalogues defined in the settings."""
        for key, value in self.parameters.items():
            if "catalogue" in key.lower():
                value_list = value.split()
                type_ = value_list[0].upper()
                endpoint = value_list[1]
                user = value_list[2] if len(value_list) > 2 else None
                password = value_list[3] if len(value_list) > 3 else None
                catalogue_interface = cswinterface.get_catalogue(
                    cswinterface.CatalogueType[type_], endpoint,
                    user=user, password=password
                )
                yield catalogue_interface


class OldMetadataCreator(GioPackage):

    def __init__(self, *args, **kwargs):
        """This class introduces two new instance variables. They are used
        only by the :py:meth:`run_post_execution` method and are useful
        mainly for testing purposes.

        * send_to_catalog, which is a boolean specifying if the outputs 
          of execution are to be sent to the csw catalog. It defaults to
          True.

        * delete_local_outputs, which is a boolean specifying if the newly
          generated execution outputs should be deleted from the local file
          system. This option is useful when outputs are sent to csw catalog.
          It defaults to True.
        """

        super(OldMetadataCreator, self).__init__(*args, **kwargs)
        self.send_to_catalog = True
        self.delete_local_outputs = True

    def execute(self, fetched):
        generator = metadata.MetadataGenerator()
        xml_dir = os.path.join(self.working_dir, 'xml')
        self.host.create_directory_tree(xml_dir)
        for gio_file, file_path in fetched.iteritems():
            generator.generate_metadata(file_path)
        result = True
        details = ''
        return result, details

    def run_pre_execution(self):
        super(OldMetadataCreator, self).run_pre_execution()
        self.compress_outputs = False

    def run_post_execution(self):
        details = ''
        result = True
        if self.send_to_catalog:
            xmls = []
            found = self.find_outputs()
            for out, found_info in found.iteritems():
                found_host, path_list = found_info
                path = out.selection_rule.filter_paths(path_list)
                xmls.append(path)
            if self.send_to_catalog == "pycsw":
                result, details = self._send_to_pycsw(xmls)
            else:  # send to geonetwork
                result, details = self._send_to_geonetwork(xmls)
        if self.delete_local_outputs:
            self.clean()
        return result, details

    def _send_to_pycsw(self, xml_paths):
        catalogue_endpoint = "http://localhost:8000/"
        csw_interface = cswinterface.CswInterfacePycsw(catalogue_endpoint)
        result, details = csw_interface.insert_records(xml_paths)
        return result, details

    def _send_to_geonetwork(self, xml_paths):
        ws = get_web_server()
        base = "http://{}.meteo.pt/{}".format(ws.host.name,
                                              ws.catalogue_base_uri)
        catalogue_endpoint = "/".join((base, ws.catalogue_csw_search_uri))
        transaction_endpoint = "/".join(
            (base, ws.catalogue_csw_transaction_uri))
        login_url = "/".join((base, "j_spring_security_check"))
        logout_url = "/".join((base, "j_spring_security_logout"))
        csw_interface = cswinterface.CswInterfaceGeonetwork(
            transaction_endpoint, login_url, logout_url,
            catalogue_endpoint
        )
        result, details = csw_interface.insert_records(xml_paths)
        return result, details
