"""
This module holds classes for creating quicklooks for giosystem products.
"""

from __future__ import absolute_import
import logging

from .basepackage import GioPackage
from ..tools import quicklooks

logger = logging.getLogger(__name__)


class StaticQuicklookBase(GioPackage):
    """
    A class for creating static quicklooks from GIO-GL products.

    This class can use the extra parameter *extra_hosts* in order to designate
    additional hosts where the output quicklooks should be put. This
    extra parameter can be set in the online settings app, in the package's
    settings.
    """

    def __init__(self, *args, **kwargs):
        super(StaticQuicklookBase, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def execute(self, fetched=None):
        """Generate quicklook files for the input product files.

        Inputs are assumed to be in the working directory in a
        decompressed and ready to process state.
        """
        
        band = self.parameters.get("band")
        layer = self.parameters.get("layer")
        
        quicklook_generator = quicklooks.QuicklookGenerator()
        for gio_file, path in fetched.items():
            quicklook_generator.create_quicklook_from_netcdf(path, band, layer)
        return True, ""
