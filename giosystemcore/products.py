'''
This module holds the GIOProduct base class and subclasses for the giosystem
package.
'''

from giosystemcore.settings import get_settings

class GioProduct(object):

    def __init__(self, name):
        self.name = name
        self.short_name = ''
        self.description = ''
        self.version = ''
        self.lineage = ''
        self.downloadable_files = dict()
        self.iso_19115_topic_categories = []
        self.inspire_data_theme = ''
        self.epsg_code = 0
        self.pixel_size = 0
        self.sdi_service_id = ''
        self.sdi_parent_id = ''
        self.temporal_extent_description = ''
        self.credit = ''
        self.geotiff_dtype = ''
        self.keywords = {'gemet': [], 'other': []}
        self.graphic_overview = {'description': '', 'type': ''}
        self.document_urls = {
            'user_manual': '',
            'validation_report': '',
            'supplemental_info': '',
        }
        self.palette = []
        self.datasets = []

    @classmethod
    def from_settings(cls, short_name):
        '''
        Create a product instance.

        Inputs:

            short_name - A string with the short name of the product
        '''

        manager = get_settings()
        product_settings = manager.get_product_settings_by_name(short_name)
        p = cls(product_settings['name'])
        p.short_name = short_name
        p.description = product_settings.get('description', '')
        for file_settings in product_settings['downloadable_files']:
            p.downloadable_files[file_settings['name']] = {
                'file_type': file_settings['file_type'],
            }
        p.version = product_settings.get('version', '')
        p.point_of_contact = product_settings.get('point_of_contact', dict())
        p.distributor = product_settings.get('distributor', dict())
        p.principal_investigator = product_settings.get(
            'principal_investigator', dict()
        )
        p.owner = product_settings.get('owner', dict())
        p.originator = product_settings.get('originator', dict())
        p.responsible = product_settings.get('responsible', dict())
        p.lineage = product_settings.get('lineage', '')
        iso_cats = product_settings.get('iso_19115_topic_categories', '')
        p.iso_19115_topic_categories = [t.strip() for t in iso_cats.split(',')]
        p.inspire_data_theme = product_settings['inspire_data_theme']
        p.epsg_code = product_settings['coordinate_reference_system_epsg_code']
        p.pixel_size = product_settings.get('pixel_size', 0)
        p.sdi_service_id = product_settings.get('sdi_service_id', '')
        p.sdi_parent_id = product_settings.get('parent_identifier', '')
        p.temporal_extent_description = product_settings.get('temporal_extent',
                                                             '')
        p.credit = product_settings.get('credit', '')
        p.purpose = product_settings.get('purpose', '')
        p.geotiff_dtype = product_settings.get('geotiff_dtype', '')
        gemet_kw = product_settings.get('gemet_keywords', '')
        other_kw = product_settings.get('other_keywords', '')
        p.keywords = {
            'gemet': [t.strip() for t in gemet_kw.split(',')],
            'other': [t.strip() for t in other_kw.split(',')],
        }
        p.graphic_overview = {
            'description': product_settings.get('graphic_overview_description',
                                                ''),
            'type': product_settings.get('graphic_overview_type', ''),
        }
        p.document_urls = {
            'user_manual': product_settings.get('user_manual_url', ''),
            'validation_report': product_settings.get('validation_report_url',
                                                      ''),
            'supplemental_info': product_settings.get('supplemental_info', ''),
        }
        palette = manager.get_palette_settings(product_settings['palette'])
        for param, value in palette.iteritems():
            if param == 'rules':
                for rule in value:
                    p.palette.append({
                        'expression': rule['expression'],
                        'color': (rule['red'], rule['green'], rule['blue']),
                    })
        p.datasets = product_settings.get('datasets', [])
        return p

    def __repr__(self):
        return '%r(name=%r)' % (self.__class__.__name__, self.short_name)
