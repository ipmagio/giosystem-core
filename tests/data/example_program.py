'''
A simple program used to test the GIOLocalHost's ability to run external
programs.
'''

import sys
import time


class FlushFile(object):
   '''Write-only flushing wrapper for file-type objects.'''

   def __init__(self, f):
       self.f = f

   def write(self, x):
       self.f.write(x)
       self.f.flush()

if __name__ == '__main__':
    sys.stdout = FlushFile(sys.__stdout__)
    print('Running the python test program...')
    iterations = 2
    sleep_seconds = 1
    for current in range(iterations):
        print('iteration %i/%i' % (current+1, iterations))
        time.sleep(sleep_seconds)
    print('Done!')
