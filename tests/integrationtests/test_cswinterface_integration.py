"""Integration tests for cswinterface module."""

import pytest
from lxml import etree


@pytest.mark.integration
class TestCswInterfaceIntegration(object):

    def test_get_records_finds_sample(self, catalogue_interface_integration,
                                      catalogue_sample_record):
        # first insert the record into the catalogue
        catalogue_interface_integration.insert_records(
            [catalogue_sample_record])
        # now verify that it can be found
        sample_id = self._get_sample_id(catalogue_interface_integration,
                                        catalogue_sample_record)
        found = catalogue_interface_integration.get_records([sample_id])
        assert len(found) == 1
        # now delete the record from the catalogue, in order to keep it clean
        catalogue_interface_integration.delete_records([sample_id])

    def test_insert_records_inserts_sample(self,
                                           catalogue_interface_integration,
                                           catalogue_sample_record):
        result, details = catalogue_interface_integration.insert_records(
            [catalogue_sample_record])
        assert result == True
        assert details == "Inserted 1 records in the catalogue"
        # now delete the record from the catalogue, in order to keep it clean
        sample_id = self._get_sample_id(catalogue_interface_integration,
                                        catalogue_sample_record)
        catalogue_interface_integration.delete_records([sample_id])

    def test_insert_records_does_not_insert_repeated_sample(
            self, catalogue_interface_integration,
            catalogue_sample_record):
        first, first_details = catalogue_interface_integration.insert_records(
            [catalogue_sample_record])
        second, details = catalogue_interface_integration.insert_records(
            [catalogue_sample_record])
        assert second == False
        # now delete the record from the catalogue, in order to keep it clean
        # this is not what we are testing here
        sample_id = self._get_sample_id(catalogue_interface_integration,
                                        catalogue_sample_record)
        catalogue_interface_integration.delete_records([sample_id])

    def test_delete_records_deletes_sample(self,
                                           catalogue_interface_integration,
                                           catalogue_sample_record):
        # first insert the sample (not what we are testing here)
        catalogue_interface_integration.insert_records(
            [catalogue_sample_record])
        # now delete it
        sample_id = self._get_sample_id(catalogue_interface_integration,
                                        catalogue_sample_record)
        result, details = catalogue_interface_integration.delete_records(
            [sample_id])
        assert result == True

    def test_replace_record_replaces_sample(self,
                                            catalogue_interface_integration,
                                            catalogue_sample_record):
        # first insert the sample (not what we are testing here)
        catalogue_interface_integration.insert_records(
            [catalogue_sample_record])
        # now replace it
        result, details = catalogue_interface_integration.replace_record(
            catalogue_sample_record)
        assert result==True
        # now delete the record from the catalogue, in order to keep it clean
        # this is not what we are testing here
        sample_id = self._get_sample_id(catalogue_interface_integration,
                                        catalogue_sample_record)
        catalogue_interface_integration.delete_records([sample_id])

    def _get_sample_id(self, catalogue_interface, catalogue_sample_record):
        """This is just a little helper function, not a test."""
        sample_element = etree.fromstring(
            catalogue_sample_record.encode("utf-8"))
        sample_id = sample_element.xpath(
            "gmd:fileIdentifier/gco:CharacterString/text()",
            namespaces=catalogue_interface.nsmap
        )[0]
        return sample_id

