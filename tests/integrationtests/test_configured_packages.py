"""Integration tests for the preparelatestwms module."""

import pytest

pytestmark = pytest.mark.integration


class TestUpdatedPrepareWmsLatestLst(object):

    def test_run_local_only(self, latest_wms_package):
        result, details = latest_wms_package.run()
        assert result

    def test_run_real(self, latest_wms_package):
        latest_wms_package.extra_hosts_to_copy_outputs = ["geoland2"]
        latest_wms_package.use_archive_for_searchin = True
        latest_wms_package.use_io_buffer_for_searching = False
        result, details = latest_wms_package.run()
        assert result


class TestStaticQuicklookBase(object):

    def test_lst_run_local_only(self, create_quicklooks_lst_package):
        result, details = create_quicklooks_lst_package.run()
        assert result
