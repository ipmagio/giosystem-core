#from __future__ import division
#
#import unittest
#import cdo
#from netCDF4 import Dataset
#
#from giosystemcore.tools.customization import netcdfcustomizer
#
#
#class TestNetcdfCustomizer(unittest.TestCase):
#
#    @classmethod
#    def setUpClass(cls):
#        cls.cdo_manager = cdo.Cdo()
#        cls.cdo_manager.setCdo("/home/geo2/programas/bin/cdo")
#        cls.original_product = ("/home/geo2/Desktop/cdo_tests/input/"
#                                "g2_BIOPAR_LST_201512200800_GLOBE_GEO_"
#                                "V1.2.nc")
#        cls.output_dir = "/home/geo2/Desktop/cdo_tests/automated"
#        cls.custom_variables = ["LST", "Q_FLAGS"]
#        cls.custom_roi = netcdfcustomizer.CdoLonLatBox(first_lon=-3.36,
#                                                       last_lon=2.51,
#                                                       first_lat=2.65,
#                                                       last_lat=22.21)
#
#    def test_netcdfcustomizer_without_options(self):
#        """Customizer works OK when no options are given."""
#        output = netcdfcustomizer.customize_product(self.original_product)
#        self.assertEqual(output, self.original_product)
#
#    def test_netcdfcustomizer_with_variables(self):
#        """Customizer extracts variables from the input NetCDF product."""
#        output = netcdfcustomizer.customize_product(
#            self.original_product, variables=self.custom_variables,
#            cdo_manager=self.cdo_manager, output_dir=self.output_dir)
#        expected_path = (
#            "{}/g2_BIOPAR_LST_201512200800_GLOBE_GEO_"
#            "V1.2_{}.nc".format(self.output_dir,
#                                "_".join(self.custom_variables))
#        )
#        self.assertEqual(output, expected_path)
#        ds = Dataset(output)
#        for variable_name in self.custom_variables:
#            self.assertIn(variable_name, ds.variables.keys())
#            self.assertEqual(ds.variables[variable_name].grid_mapping, "crs")
#        ds.close()
#
#    def test_netcdfcustomizer_with_roi(self):
#        """Customizer extracts a spatial region of interest."""
#        output = netcdfcustomizer.customize_product(
#                self.original_product, region_of_interest=self.custom_roi,
#                cdo_manager=self.cdo_manager, output_dir=self.output_dir)
#        expected_path = (
#            "{}/g2_BIOPAR_LST_201512200800_GLOBE_GEO_"
#            "V1.2_W3.36N2.65_E2.51N22.21.nc".format(self.output_dir)
#        )
#        self.assertEqual(output, expected_path)
#        ds = Dataset(output)
#        lats = ds.variables["lat"]
#        lons = ds.variables["lon"]
#        half_resolution = round(abs(lats[1] - lats[0]), 4) / 2
#        first_lon = round(lons[0] - half_resolution)
#        last_lon = round(lons[-1] + half_resolution)
#        first_lat = round(lats[0] - half_resolution)
#        last_lat = round(lats[-1] + half_resolution)
#        self.assertEqual(first_lon, round(self.custom_roi.first_lon))
#        self.assertEqual(last_lon, round(self.custom_roi.last_lon))
#        self.assertEqual(last_lat, round(self.custom_roi.first_lat))
#        self.assertEqual(first_lat, round(self.custom_roi.last_lat))
#        ds.close()
#
#    def test_netcdfcustomizer_with_roi_and_variables(self):
#        """Customizer extracts a spatial region of interest and variables."""
#        output = netcdfcustomizer.customize_product(
#                self.original_product, variables=self.custom_variables,
#                region_of_interest=self.custom_roi,
#                cdo_manager=self.cdo_manager, output_dir=self.output_dir)
#        expected_path = (
#            "{}/g2_BIOPAR_LST_201512200800_GLOBE_GEO_"
#            "V1.2_{}_W3.36N2.65_E2.51N22.21.nc".format(
#                self.output_dir, "_".join(self.custom_variables))
#        )
#        self.assertEqual(output, expected_path)
