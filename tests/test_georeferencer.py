'''
Unit tests for the georeferencer module.
'''

import unittest

from mock import Mock, patch
import tables
import numpy as np

import giosystemcore.tools.georeferencer as georef

class TestHdf5TileGeoreferencer(unittest.TestCase):

    def setUp(self):
        self.g = georef.Hdf5TileGeoreferencer()

    def test_create_geotransform(self):
        self.g.first_lon = 10
        self.g.first_lat = 20
        self.g.pixel_size = 0.05
        target = [9.975, 0.05, 0, 20.025, 0, -0.05]
        result = self.g.create_geotransform()
        self.assertEqual(result, target)

    def test_get_pixel_size(self):
        possible_values = [
            '0.05 x 0.05 (lon x lat) degrees',
        ]
        for value in possible_values:
            self.assertTrue(self.g._get_pixel_size(value), 0.05)

    def test_get_path_parts(self):
        p = '/home/dummy/path/fake_file.rst'
        parts = self.g._get_path_parts(p)
        self.assertTupleEqual(parts, ('/home/dummy/path', 'fake_file', 'rst'))

    def test_rescale_data(self):
        d = np.array([
            [-8000, 200, 200, 500],
            [500, 500, -8000, 200]
        ], dtype=self.g.NUMPY_D_TYPE)
        target = np.array([
                         [-8000, 2, 2, 5],
                         [5, 5, -8000, 2]
        ])
        missing = -8000
        scaling = 100
        rescaled = self.g._rescale_data(d, missing, scaling)
        print('rescaled: {}'.format(rescaled))
        self.assertTrue(np.all(np.equal(rescaled,target)))

    @patch('tables.array.Array')
    @patch('tables.file.File')
    def test_get_attributes(self, mocked_file, mocked_array):
        ds_mocked = mocked_file.return_value
        ds_mocked.root._v_attrs = {
            'NL': '10',
            'NC': '20',
            'FIRST_LON': '0',
            'FIRST_LAT': '0',
            'PIXEL_SIZE': '0.05 x 0.05',
            'PRODUCT': 'DUMMY'
        }
        array_mocked = mocked_array.return_value
        array_mocked.name = 'DUMMY'
        array_mocked._v_attrs = {
            'MISSING_VALUE': '-8000',
            'SCALING_FACTOR': '100'
        }
        ds_mocked.iter_nodes.return_value = [array_mocked]
        mocked_openFile = Mock()
        mocked_openFile.return_value = ds_mocked
        tables.openFile = mocked_openFile
        attrs = self.g._get_attributes()
        self.assertTupleEqual(attrs, (
            10, 20, 0, 0, 0.05,
            {'DUMMY': {'missing_value':-8000, 'scaling_factor': 100}},
            'DUMMY'
        ))

    @patch('tables.array.Array')
    @patch('tables.file.File')
    def test_get_data(self, mocked_file, mocked_array):

        target = {
            'DUMMY': np.array([
                [100, 222],
                [222, 350]
            ]),
            }
        array_mocked = mocked_array.return_value
        array_mocked.read.return_value = target['DUMMY']
        ds_mocked = mocked_file.return_value
        ds_mocked.get_node.return_value = array_mocked
        mocked_openFile = Mock()
        mocked_openFile.return_value = ds_mocked
        tables.openFile = mocked_openFile
        result = self.g.get_data(*target.keys())
        self.assertTrue(np.all(np.equal(target['DUMMY'], result['DUMMY'])))





if __name__ == '__main__':
    pass
