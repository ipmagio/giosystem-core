"""pytest configuration file for testing giosystemcore.packages"""

import pytest

from giosystemcore.packages import metadata


@pytest.fixture()
def metadata_package(request):
    package = metadata.MetadataCreator("test_metadata_creator")
