"""Unit tests for giosystemcore.tools.netcdfanalyzer."""

import datetime as dt

import pytest
import mock

from giosystemcore.tools import netcdfanalyzer


@pytest.mark.unit
class TestProductInfo(object):

    def test_extract_timeslot_with_days_step_0(self):
        fake_date = dt.datetime(2015, 1, 1)
        fake_step = 0
        with mock.patch("giosystemcore.tools.netcdfanalyzer.netCDF4.Variable",
                        autospec=True) as mocks:
            fake_time_variable = mocks.return_value
            fake_time_variable.units = ("days since {:%Y-%m-%d "
                                        "%H:%M:%S}".format(fake_date))
            extracted = netcdfanalyzer.ProductInfo.extract_timeslot(
                fake_time_variable,
                step=fake_step
            )
            assert extracted == fake_date

    def test_extract_timeslot_with_days_step_1(self):
        fake_date = dt.datetime(2015, 1, 1)
        fake_step = 1
        with mock.patch("giosystemcore.tools.netcdfanalyzer.netCDF4.Variable",
                        autospec=True) as mocks:
            fake_time_variable = mocks.return_value
            fake_time_variable.units = ("days since {:%Y-%m-%d "
                                        "%H:%M:%S}".format(fake_date))
            extracted = netcdfanalyzer.ProductInfo.extract_timeslot(
                fake_time_variable,
                step=fake_step
            )
            assert extracted == fake_date + dt.timedelta(days=fake_step)

    def test_extract_timeslot_with_minutes_step_0(self):
        fake_date = dt.datetime(2015, 1, 1, 10)
        fake_step = 0
        with mock.patch("giosystemcore.tools.netcdfanalyzer.netCDF4.Variable",
                        autospec=True) as mocks:
            fake_time_variable = mocks.return_value
            fake_time_variable.units = ("minutes since {:%Y-%m-%d "
                                        "%H:%M:%S}".format(fake_date))
            extracted = netcdfanalyzer.ProductInfo.extract_timeslot(
                fake_time_variable,
                step=fake_step
            )
            assert extracted == fake_date

    def test_extract_timeslot_with_minutes_step_30(self):
        fake_date = dt.datetime(2015, 1, 1, 10)
        fake_step = 30
        with mock.patch("giosystemcore.tools.netcdfanalyzer.netCDF4.Variable",
                        autospec=True) as mocks:
            fake_time_variable = mocks.return_value
            fake_time_variable.units = ("minutes since {:%Y-%m-%d "
                                        "%H:%M:%S}".format(fake_date))
            extracted = netcdfanalyzer.ProductInfo.extract_timeslot(
                fake_time_variable,
                step=fake_step
            )
            assert extracted == fake_date + dt.timedelta(minutes=fake_step)

    def test_extract_contact_details(self):
        # this expression was copied from an actual netcdf product
        fake_expression = (
            u"Principal investigator (Researcher): isabel.trigo@ipma.pt; "
            u"Instituto Portugu\xeas do Mar e da Atmosfera (IPMA); Rua C ao "
            u"Aeroporto; Lisbon; 1749-077; Portugal (PT); IPMA website; "
            u"http://www.ipma.pt\n"
            u"Originator (IPMA GIO-Global Land Help Desk): "
            u"sandra.coelho@ipma.pt; Instituto Portugu\xeas do Mar e da "
            u"Atmosfera (IPMA); Rua C ao Aeroporto; Lisbon; 1749-077; "
            u"Portugal (PT); IPMA website; http://www.ipma.pt\nPoint of "
            u"contact (GIO-Global Land Help Desk): helpdesk@vgt.vito.be; "
            u"Flemish Institute for Technological Research (VITO); Boeretang "
            u"200; Mol; 2400; Belgium (BE); VITO website; "
            u"http://land.copernicus.eu/global/\n"
            u"Owner: ENTR-COPERNICUS-ASSETS@ec.europa.eu; European Commission "
            u"Directorate-General for Enterprise and Industry (EC-DGEI); "
            u"Avenue d'Auderghem 45; Brussels; 1049; Belgium (BE); EC-DGEI "
            u"website; http://ec.europa.eu/enterprise/\n"
            u"Custodian (Responsible): "
            u"copernicuslandproducts@jrc.ec.europa.eu; European Commission "
            u"Directorate-General Joint Research Center (JRC); Via E.Fermi, "
            u"249; Ispra; 21027; Italy (IT); JRC "
            u"website; http://ies.jrc.ec.europa.eu"
        )
        with mock.patch("giosystemcore.tools.netcdfanalyzer.ContactDetails",
                        autospec=True) as mocked:
            details = netcdfanalyzer.ProductInfo.extract_contact_details(
                fake_expression)
            assert len(details) == len(fake_expression.splitlines())
            last_args, last_kwargs = mocked.call_args_list[-1]
            expected_kwargs = {
                "name": ("European Commission Directorate-General Joint "
                         "Research Center"),
                "short_name": "JRC",
                "position": "Responsible",
                "delivery_point": "Via E.Fermi, 249",
                "city": "Ispra",
                "postal_code": "21027",
                "country": "Italy",
                "country_code": "IT",
                "e_mail": "copernicuslandproducts@jrc.ec.europa.eu",
                "website": "http://ies.jrc.ec.europa.eu",
                "role": "Custodian",
            }
            assert last_kwargs == expected_kwargs

    def test_extract_geographic_bounds(self):
        fake_left_lon = 0
        fake_right_lon = 40
        fake_upper_lat = 30
        fake_lower_lat = -20
        fake_lat = [fake_upper_lat, fake_lower_lat]
        fake_lon = [fake_left_lon, fake_right_lon]
        expected_x_resolution = abs(fake_lower_lat - fake_upper_lat)
        expected_y_resolution = abs(fake_right_lon - fake_left_lon)

        ds_path = "giosystemcore.tools.netcdfanalyzer.netCDF4.Dataset"
        bb_path = "giosystemcore.tools.netcdfanalyzer.BoundingBox"

        with mock.patch(ds_path, autospec=True) as mocked_ds, \
                mock.patch(bb_path, autospec=True) as mocked_bb:
            mock_dataset = mocked_ds.return_value
            mock_dataset.variables = {
                "lat": fake_lat,
                "lon": fake_lon,
            }
            extracted = netcdfanalyzer.ProductInfo.extract_geographic_bounds(
                mock_dataset)
            mocked_bb.assert_called_with(
                upper_left_lon=fake_left_lon + expected_y_resolution / 2,
                upper_left_lat=fake_upper_lat - expected_x_resolution / 2,
                lower_right_lon=fake_right_lon - expected_y_resolution / 2,
                lower_right_lat=fake_lower_lat + expected_x_resolution / 2
            )
            assert extracted[1] == expected_x_resolution

