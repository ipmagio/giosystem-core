"""Unit tests for cswinterface module."""

import mock
from lxml import etree
import pytest

from giosystemcore.catalogue import cswinterface


@pytest.mark.unit
def test_get_catalogue_geonetwork():
    catalogue_type = cswinterface.CatalogueType.GEONETWORK
    catalogue_endpoint = "fake_endpoint"
    user = "fake_user"
    password = "fake_password"
    iface = cswinterface.get_catalogue(catalogue_type, catalogue_endpoint,
                                       user=user, password=password)
    assert isinstance(iface, cswinterface.CswInterfaceGeonetwork)


@pytest.mark.unit
def test_get_catalogue_pycsw():
    catalogue_type = cswinterface.CatalogueType.PYCSW
    catalogue_endpoint = "fake_endpoint"
    user = "fake_user"
    password = "fake_password"
    iface = cswinterface.get_catalogue(catalogue_type, catalogue_endpoint,
                                       user=user, password=password)
    assert isinstance(iface, cswinterface.CswInterfacePycsw)


@pytest.mark.unit
def test_get_catalogue_invalid_type():
    catalogue_type = "invalid"
    catalogue_endpoint = "fake_endpoint"
    with pytest.raises(TypeError):
        iface = cswinterface.get_catalogue(catalogue_type,
                                           catalogue_endpoint)


@pytest.mark.unit
class TestCswInterface(object):

    def test_get_records(self, catalogue_interface):
        expected_id = "phony_id"
        mock_response = mock.MagicMock(
            "giosystemcore.catalogue.cswinterface.requests.Response",
            autospec=True
        ).return_value
        mock_response.status_code = 200
        mock_response.text = """
            <csw:GetRecordByIdResponse xmlns:csw='{csw}'>
              <csw:SummaryRecord xmlns:dc='{dc}'>
                <dc:identifier>{fake_id}</dc:identifier>
              </csw:SummaryRecord>
            </csw:GetRecordByIdResponse>
        """.format(fake_id=expected_id, **catalogue_interface.nsmap)
        catalogue_interface._session.get.return_value = mock_response
        output = catalogue_interface.get_records([expected_id])
        assert output[0] == expected_id

    def test_insert_records_single_record(self, catalogue_interface):
        record_tags = ["fakeRecord"]
        records = ["<{}/>".format(tag) for tag in record_tags]
        mock_response = mock.MagicMock(
            "giosystemcore.catalogue.cswinterface.requests.Response",
            autospec=True
        ).return_value
        mock_response.text = """
            <csw:TransactionResponse xmlns:csw='{csw}'>
              <csw:TransactionSummary>
                <csw:totalInserted>1</csw:totalInserted>
              </csw:TransactionSummary>
            </csw:TransactionResponse>
        """.format(**catalogue_interface.nsmap)
        catalogue_interface._session.post.return_value = mock_response
        insert_result, details = catalogue_interface.insert_records(
            records=records)
        call_args = catalogue_interface._session.post.call_args_list
        assert len(call_args) == 1 or len(call_args) == 4
        if len(call_args) == 1:
            # only one request, no logout/login cycle
            used_args, used_kwargs = call_args[0]
        else:  # len(call_args) == 4
            # 1.logout -> 2.login() -> 3.request() -> 4.logout()
            used_args, used_kwargs = call_args[2]
        used_data = etree.fromstring(used_kwargs["data"])
        sent_records = used_data.xpath("csw:Insert/{}".format(record_tags[0]),
                                       namespaces=catalogue_interface.nsmap)
        assert insert_result == True
        assert used_args[0] == catalogue_interface.transaction_endpoint
        assert used_kwargs["headers"] == catalogue_interface.request_headers
        assert len(sent_records) == len(record_tags)

    def test_delete_records_single_record(self, catalogue_interface):
        record_id = "fake_id"
        mock_response = mock.MagicMock(
            "requests.Response", autospec=True).return_value
        mock_response.text = """
            <csw:TransactionResponse xmlns:csw='{csw}'>
              <csw:TransactionSummary>
                <csw:totalDeleted>1</csw:totalDeleted>
              </csw:TransactionSummary>"
            </csw:TransactionResponse>
        """.format(**catalogue_interface.nsmap)
        catalogue_interface._session.post.return_value = mock_response
        delete_result, details = catalogue_interface.delete_records(
            [record_id])
        call_args = catalogue_interface._session.post.call_args_list
        assert len(call_args) == 1 or len(call_args) == 4
        if len(call_args) == 1:
            # only one request, no logout/login cycle
            used_args, used_kwargs = call_args[0]
        else:  # len(call_args) == 4
            # 1.logout -> 2.login() -> 3.request() -> 4.logout()
            used_args, used_kwargs = call_args[2]
        used_data = etree.fromstring(used_kwargs["data"])
        used_property_name = used_data.xpath(
            "csw:Delete/csw:Constraint/ogc:Filter/ogc:PropertyIsEqualTo/"
            "ogc:PropertyName/text()", namespaces=catalogue_interface.nsmap
        )[0]
        used_id = used_data.xpath(
            "csw:Delete/csw:Constraint/ogc:Filter/ogc:PropertyIsEqualTo/"
            "ogc:Literal/text()", namespaces=catalogue_interface.nsmap
        )[0]
        assert delete_result == True
        assert used_args[0] == catalogue_interface.transaction_endpoint
        assert used_kwargs["headers"] == catalogue_interface.request_headers
        assert used_property_name == "apiso:Identifier"
        assert used_id == record_id

    def test_replace_record_existing_record(self, catalogue_interface):
        """Catalogue interface is able to replace an already existing record.
        """
        record_id = "fake_id"
        record = """
            <gmd:MD_Metadata xmlns:gmd='{gmd}' xmlns:gco='{gco}'>
              <gmd:fileIdentifier>
                <gco:CharacterString>{record_id}</gco:CharacterString>
              </gmd:fileIdentifier>
            </gmd:MD_Metadata>
        """.format(record_id=record_id, **catalogue_interface.nsmap)

        with mock.patch.multiple(catalogue_interface,
                                 get_records=mock.DEFAULT,
                                 delete_records=mock.DEFAULT,
                                 insert_records=mock.DEFAULT,
                                 autospec=True) as mocks:
            mocked_get_records = mocks["get_records"]
            mocked_delete_records = mocks["delete_records"]
            mocked_insert_records = mocks["insert_records"]
            mocked_get_records.return_value = [(record_id, "")]
            mocked_delete_records.return_value = (True, "")
            mocked_insert_records.return_value = (True, "")
            replace_result, details = catalogue_interface.replace_record(record)
            assert replace_result == True
