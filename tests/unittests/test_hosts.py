"""Unit tests for giosystemcore.hosts.hosts"""

import socket

from pathlib2 import Path
import pytest
import mock

from giosystemcore.hosts import hosts
from giosystemcore.hosts import hostfactory
import giosystemcore.hosts.hostroles

#LOCAL_DIR = os.path.dirname(os.path.realpath(__file__))
#local_host_name = socket.gethostname()
#SETTINGS = {
#    'hosts': {
#        local_host_name: {
#            'active': True,
#            'code_dir': u'',
#            'data_dir': u'',
#            'description': u'',
#            'ip': u'',
#            'password': u'',
#            'resource_url': u'',
#            'temp_dir': u'',
#            'username': u'',
#        }
#    }
#}


@pytest.mark.unit
class TestGioLocalHost:

    def test_creation_with_dirs(self):
        fake_name = "fake"
        fake_data_dirs = ["/no/data"]
        fake_code_dir = "/no/code"
        fake_temp_dir = "/no/temp"
        h = hosts.GioLocalHost(
            fake_name,
            data_dirs=fake_data_dirs,
            code_dir=fake_code_dir,
            temp_dir=fake_temp_dir
        )
        assert h.name == fake_name
        assert h.data_dirs == fake_data_dirs
        assert h.code_dir == fake_code_dir
        assert h.temp_dir == fake_temp_dir

    def test_creation_no_dirs(self):
        fake_name = "fake"
        h = hosts.GioLocalHost(fake_name)
        assert h.name == fake_name
        assert h.data_dirs == [str(Path.home() / "data")]
        assert h.code_dir == str(Path.home() / "code")
        assert h.temp_dir == "/tmp/giosystem"

    def test_from_settings(self):
        fake_name = "fake"
        fake_settings = {
            "ip": "fake_ip",
            "username": "fake_username",
            "password": "fake_password",
            "data_dir": "fake_data_dir",
            "code_dir": "fake_code_dir",
            "temp_dir": "fake_temp_dir",
            "description": "fake_description",
            "domain": "fake_domain",
        }
        with mock.patch("giosystemcore.hosts.hosts.get_settings",
                        autospec=True) as mock_get_settings:
            mock_get_settings.return_value.get_host_settings.return_value = (
                fake_settings)
            h = hosts.GioLocalHost.from_settings(fake_name)
            assert mock_get_settings.call_count == 1
            assert h.name == fake_name
            assert h.ip == fake_settings["ip"]
            assert h.username == fake_settings["username"]
            assert h.password == fake_settings["password"]
            assert h.data_dirs[0] == fake_settings["data_dir"]
            assert h.code_dir == fake_settings["code_dir"]
            assert h.temp_dir == fake_settings["temp_dir"]
            assert h.description == fake_settings["description"]
            assert h.domain == fake_settings["domain"]

    @pytest.mark.xfail
    def test_create_file(self):
        raise NotImplementedError

    @pytest.mark.xfail
    def test_create_directory_tree(self):
        raise NotImplementedError

    @pytest.mark.xfail
    def test_compress(self):
        raise NotImplementedError

    @pytest.mark.xfail
    def test_create_zip(self):
        raise NotImplementedError

    @pytest.mark.xfail
    def test_decompress(self):
        raise NotImplementedError

    @pytest.mark.xfail
    def test_dir_exists(self):
        raise NotImplementedError

    @pytest.mark.xfail
    def test_find(self):
        raise NotImplementedError

    @pytest.mark.xfail
    def test_fetch(self):
        raise NotImplementedError

    @pytest.mark.xfail
    def test_is_empty_dir(self):
        raise NotImplementedError

    @pytest.mark.xfail
    def test_run_program(self):
        raise NotImplementedError



# the following are integration tests not unit. And they need to be refactored
# to work with pytest.

#class GIOLocalHostTests:
#
#    def setUp(self):
#        self.host = hf.get_host(the_settings=SETTINGS)
#
#    def test_create_local_host(self):
#        self.assertIsInstance(self.host,
#                              giosystemcore.hosts.hosts.GIOLocalHost)
#
#    def test_run_program_python(self):
#        test_program = LOCAL_DIR + '/data/example_program.py'
#        command = 'python %s' % test_program
#        output, result = self.host.run_program(command)
#        self.assertIs(result, True)
#
#    def test_run_program_bash(self):
#        test_program = LOCAL_DIR + '/data/example_program.sh'
#        command = 'bash %s' % test_program
#        output, result = self.host.run_program(command)
#        self.assertIs(result, True)
#
#    def test_run_program_fortran(self):
#        program_dir = os.path.join(LOCAL_DIR, 'data')
#        source_name = 'example_program.f90'
#        bin_name = 'test_program_fortran'
#        call(['gfortran', '-o', os.path.join(program_dir, bin_name),
#             os.path.join(program_dir, source_name)])
#        command = os.path.join(program_dir, bin_name)
#        output, result = self.host.run_program(command)
#        self.assertIs(result, True)
#
#    def test_run_program_cpp(self):
#        program_dir = os.path.join(LOCAL_DIR, 'data')
#        source_name = 'example_program.cpp'
#        bin_name = 'test_program_cpp'
#        call(['g++', '-std=c++11', '-o', os.path.join(program_dir, bin_name),
#             os.path.join(program_dir, source_name)])
#        command = os.path.join(program_dir, bin_name)
#        output, result = self.host.run_program(command)
#        self.assertIs(result, True)
#
#    @unittest.skip('currently broken')
#    def test_run_program_working_dir(self):
#        command = 'ls -lah'
#        working_directory = '/'
#        output, result = self.host.run_program(command)
#        self.assertIs(result, True)
#
#
#
#class GIOLocalHostIOTests(unittest.TestCase):
#
#    def setUp(self):
#        self.host = hf.get_host(the_settings=SETTINGS)
#        self.data_path = os.path.join(LOCAL_DIR, 'data')
#        self.inputs_path = os.path.join(self.data_path, 'inputs')
#        self.outputs_path = os.path.join(self.data_path, 'outputs')
#
#    def test_fetch_from_local(self):
#        path = os.path.join(self.inputs_path, 'test_file.txt')
#        new_path = self.host._fetch_from_local(path, self.outputs_path)
#        self.assertIs(os.path.isfile(new_path), True)
#
#    def tearDown(self):
#        shutil.rmtree(self.outputs_path)

#if __name__ == '__main__':
#    unittest.main()
