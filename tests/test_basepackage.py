"""
Unit tests for the basepackage module.
"""

import unittest

from mock import Mock, patch

import giosystemcore.packages.basepackage
import giosystemcore.settings


class TestGioPackage(unittest.TestCase):

    def setUp(self):
        giosystemcore.settings._settings_manager = Mock()
        self.package_settings = {
            'name': 'teste',
            'resource_uri': '',
            'category': {
                'name': 'Testing',
                'specific_host': None,
                'specific_host_role': '',
                'use_specific_host': '',
            },
            'core_class': '',
            'depends_on': '',
            'description': '',
            'ecflow_retry_frequency': 10,
            'ecflow_tries': 1,
            'exclude_hours': [],
            'execution_frequency': 'hourly',
            'external_command': '',
            'start_time_offset': 0,
            'use_specific_host': '',
            'parameters': [],
            'specific_host': None,
            'trigger_type': 'dependency',
            'specific_host_role': None,
            'inputs': [],
            'outputs': [],
        }
        self.host_settings = {
            'name': 'test_host',
            'domain': '',
            'description': '',
            'code_dir': '',
            'data_dir': '',
            'temp_dir': '',
            'ip': '',
            'username': '',
            'password': '',
            'resource_uri': '',
        }

    @patch('giosystemcore.packages.basepackage.get_settings')
    def test_stuffs(self, mocked_host_factory, mocked_get_settings):
        mocked_settings_manager = mocked_get_settings.return_value
        mocked_settings_manager.get_package_settings.return_value = \
            self.package_settings.copy()
        p = giosystemcore.packages.basepackage.GioPackage.from_settings(
            'teste')
        self.assertEquals(p.name, self.package_settings['name'])
