'''
Prepare the host to run the giosystem-core package.

This involves:

    - Installing some system-wide libraries like HDF5 and gdal.
    - creating a virtualenv and enabling those system-wide libraries' python
        interfaces inside the virtualenv
    - Enabling some other python libraries that are easier to have system-wide
        like numpy
'''

import os
import re

from fabric.api import local
from fabric.context_managers import lcd, shell_env

VENV_NAME = 'venv'
LOCAL_DIR = os.path.dirname(os.path.realpath(__file__))
VENV_PATH = os.path.join(LOCAL_DIR, VENV_NAME)
LOCAL_PIP = os.path.join(VENV_PATH, 'bin', 'pip')
LOCAL_PYTHON = os.path.join(VENV_PATH, 'bin', 'python')
REQUIREMENTS_FILE = 'requirements.txt'

def prepare_host(venv_path=None):
    '''
    Install all the needed dependencies in order to make this
    host able to execute the giosystem-core python package.
    '''

    global VENV_PATH
    global VENV_NAME
    global LOCAL_DIR
    global LOCAL_PIP
    global LOCAL_PYTHON
    if venv_path is None:
        local('virtualenv %s' % VENV_NAME)
    else:
        VENV_PATH = venv_path
        LOCAL_DIR, VENV_NAME = os.path.split(venv_path)
        LOCAL_PIP = os.path.join(VENV_PATH, 'bin', 'pip')
        LOCAL_PYTHON = os.path.join(VENV_PATH, 'bin', 'python')
    install_apt_dependencies()
    install_pip_requirements()

def install_pip_requirements():
    local('%s install -r %s' % (LOCAL_PIP, REQUIREMENTS_FILE))

def install_apt_dependencies():
    local('sudo apt-get install python-virtualenv python-pip')
