enum34  # backport of enum to python<3.4
h5py
lxml
mock
netcdf4
numpy
owslib
pathlib2  # backport of pathlib to python<3.4
paramiko
pexpect
Pillow
pysftp
pystache
requests
