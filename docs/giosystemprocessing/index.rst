Giosystem processing
====================

The giosystemprocessing package handles remotely calling a giosystemcore
package through the OGC `Web Processing Service`_ (WPS).

It is also responsible for installing the external Fortran and C/C++ code
algorithms that implement the product generation logic.

For WPS, it uses `pywps`_ as a backend.

.. _Web Processing Service: http://www.opengeospatial.org/standards/wps
.. _pywps: http://pywps.wald.intevation.org/

Installation
------------

Detailed installation instructions are available at the package's source code
repository at http://bitbucket.org/ipmagio/giosystem-processing

Installing the external code
----------------------------

Use the supplied scripts

Usage
-----

After installation, the server should be accessible at the url 
http://<server_address>/giosystem/processing/wps

Web Processing Service
----------------------

Web Processing Service is a standard for the execution of geospatial processing
services. It defines an interface that a client can use to communicate with a
processing server over the Internet and ask for a specific process to be
executed.

Processes
---------

The following processes are implemented by giosystem-processing:

gio_process
...........

This process executes a package defined in giosystem-settings-django.

It accepts as inputs:

* **package name**
* **settings uri**
* **timeslot**
* compress outputs
* copy outputs to archive
* copy outputs to io buffer
* remove package working dir
* use archive for searching
* use io_buffer for searching
* extra hosts to copy outputs

And returns as outputs:

* output result
* output details

gio_archive_process
...................

This process executes the archiving part of a package defined in 
giosystem-settings-django.

.. note:: 

   add a process for the cleaning up of package outputs
