giosystemcore.catalogue package
===============================

Submodules
----------

.. toctree::

   giosystemcore.catalogue.cswinterface
   giosystemcore.catalogue.metadata

Module contents
---------------

.. automodule:: giosystemcore.catalogue
    :members:
    :undoc-members:
    :show-inheritance:
