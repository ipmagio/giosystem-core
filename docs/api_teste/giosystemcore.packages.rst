giosystemcore.packages package
==============================

Subpackages
-----------

.. toctree::

    giosystemcore.packages.externalcode

Submodules
----------

.. toctree::

   giosystemcore.packages.basepackage
   giosystemcore.packages.fetchdata
   giosystemcore.packages.metadata
   giosystemcore.packages.packagefactory
   giosystemcore.packages.quicklooks
   giosystemcore.packages.selectionrules

Module contents
---------------

.. automodule:: giosystemcore.packages
    :members:
    :undoc-members:
    :show-inheritance:
