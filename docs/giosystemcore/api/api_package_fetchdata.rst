giosystemcore.packages.fetchdata
================================

.. autoclass:: giosystemcore.packages.fetchdata.FetchData
   :show-inheritance:
   :member-order: bysource
   :members:
