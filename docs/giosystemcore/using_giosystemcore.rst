Using giosystemcore
===================

Now that giosystemcore is installed, it can be used like any other Python
package. However, because we have installed it inside a virtualenv, we must
ensure that we use the Python interpreter from the virtualenv whenever we wish
to work with the giosystemcore library.

* For interactive sessions, be sure to activate your virtualenv before starting
  the Python interpreter.

  .. code:: bash

     cd ~/dev/giosystem
     source venv/bin/activate
     ipython

* For scripting, be sure to execute the script with the Python interpreter
  provided by your virtualenv.

  .. code:: bash

     dev/giosystem/venv/bin/python myscript.py


Lets start by importing the settings module and loading up the settings
defined in the `giosystem-settings-django` app, which is another component
of the whole giosystem.

.. code:: python

   import giosystemcore.settings

   settings_url = 'http://gio-gl.meteo.pt/giosystem/settings/api/v1/'
   giosystemcore.settings.get_settings(settings_url)

This is a crucial step in order to bind our giosystemcore session to a
specific set of settings.

The :py:func:`giosystemcore.settings.get_settings` function is responsible for 
initializing an object that interfaces with the online 
giosystem-settings-django app. This object is cached and is made available to 
all the giosystemcore modules so that the various objects can be correctly 
initialized. We can store the output of this function as a settings manager 
object, but since we don't need to interact with it directly, we can just call 
the function and discard its output.

Lets play with some of the files used in the giosystem processing algorithms.
Suppose we want to work with the cloud mask file for the GOES satellite from
the date 2014-02-24 10:00:00. This file's timeslot is from the past, so the
file has already been processed in the giosystem processing lines. As such, 
it is probably available:

* in our own local machine, OR
* in the giosystem's io_buffer machine, OR
* in the giosystem's archive

Lets try to find it and copy it over to some local working directory:

.. code:: python

   import giosystemcore.files

   f1 = giosystemcore.files.get_file('goes_cma', '201402241000')
   print(f1.name, f1.file_type, f1.year)
   print(f1.search_path)
   print(f1.search_pattern)
   # lets try to find the file locally or on the io_buffer
   found_host, found = f1.find()
   print(found)

When a file is not found locally or in the io_buffer, we can ask for it to be
searched in the archive:

.. code:: python

   found_host, found = f1.find(use_archive=True)
