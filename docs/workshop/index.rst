giosystem workshop
==================

This is a short workshop that covers the various aspects of the giosystem
software package.

Session #0 - Tools of the trade
:::::::::::::::::::::::::::::::

In this session we introduce some software tools used when developing
giosystem components.

virtualenv
----------

http://virtualenv.readthedocs.org/en/latest/

Virtualenv is a Python package that allows creating isolated Python
environments in any directory. These environments can be controlled 
independently of the system wide Python installation and can be installed
anywhere inside the user's home directory. This means that we can:

* install Python packages without requiring root access;
* install different versions of the same package without conflicts;
* install as many packages as a project needs without polluting the system wide
  Python installation with packages that are only used by a single project;
* create a virtualenv for each project, which keeps each projects libraries
  stable and isolated

In short, a virtualenv is a custom Python interpreter, located in a directory
chosen by the user, where it is possible to install project-specific packages.

Installation
............

.. code:: bash
   
   sudo apt-get install python-virtualenv

Operation
.........

In order to use a virtualenv, we must:

* create it
* activate it

Lets say we are working on project `~/dev/myproject` and wish to use
a virtualenv.

Virtual environments can be created wherever the user wishes, but usually we
create them as a sub-directory under the project that will be using the
virtualenv. They can have any name, but it is common practice to name them
`venv`.

.. code:: bash

   mkdir -p ~/dev/myproject && cd ~/dev/myproject
   virtualenv venv

Now that the virtualenv has been created, we need to activate it, so that it
can be used. There are two ways to activate and use a virtualenv:

1. When working in a terminal, we can source the `activate` script. The 
   terminal's command prompt then changes, including the name of the virtualenv
   to provide a visual cue that the virtualenv is active.

   .. code:: bash
      
      geo2@geo2:~/dev/myproject$ source venv/bin/activate
      (venv)geo2@geo2:~/dev/myproject$ # the virtualenv has been activated

   After activating a virtualenv, the `python` command will now refer to a new
   Python interpreter, located inside the virtualenv's directory. We can verify
   this by issuing the `which` command:

   .. code:: bash

      (venv)geo2@geo2:~/dev/myproject$ which python
      ~/dev/myproject/venv/bin/python

   When we no longer wish to use the virtualenv, we can issue the `deactivate`
   command and it will be deactivated. The terminal's prompt returns to its
   normal state to indicate that the virtualenv is no longer active.

   .. code:: bash

      (venv)geo2@geo2:~/dev/myproject$ deactivate
      geo2@geo2:~/dev/myproject$ # the virtualenv is no longer active

2. When running scripts and applications, we can directly use the Python
   interpreter that was created by the virtualenv instead of using the default
   system Python interpreter. For example, if we had a `myscript.py` file to
   run, we'd execute it like this:

   .. code:: bash

      # instead of calling python myscript.py
      venv/bin/python myscript.py

   In this case, the virtualenv is only used for executing the script, as soon
   as execution is terminated, so is the virtualenv.

Since a virtualenv is just a directory with custom (but isolated) Python
interpreter and packages, uninstalling it is just a matter of deleting the
virtualenv directory.

.. code:: bash

   rm -rf ~/dev/myproject/venv

pip
---

http://www.pip-installer.org/en/latest/

Pip is an installer of Python packages. It provides some very convenient
functionality:

* download and install packages from:

  * the Python Package Index (http://pypi.python.org)
  * source code repositories

* install **and** uninstall packages
* install packages from a list of requirements

Installation
............

Pip can be installed system wide, for using with the default Python
interpreter. It can also be used inside a virtualenv. In this case, the
virtualenv command takes care of installing pip for us when creating the
virtualenv, so there is no need to do anything.

.. code:: bash
   
   sudo apt-get install python-pip

Operation
.........

Using pip to install packages is straightforward. Lets try installing the
`requests` package, which is available on pypi:

.. code:: bash

   # pip install <package_name or url>
   (venv) pip install requests

Pip will download and install the packages. There are many other options
available, such as specifying the version number for a package, separating the
downloading and installing steps, etc.

Uninstalling a package is just as simple:

.. code:: bash

   (venv) pip uninstall requests

For inspectings which packages are currently installed we can use the `freeze`
command:

.. code:: bash

   (venv) pip freeze

Pip has the ability to install packages from a requirements file.
A requirements file is just a normal text file where each package is defined in
a new line. This means we can store the output of `pip freeze` into a file and
then use this file in another location to install the same packages.

.. code:: bash

   (venv) pip install requests
   (venv) pip freeze > requirements.txt
   (venv) pip uninstall requests
   (venv) pip install -r requirements.txt

This makes requirements files a nice thing to have in a source code repository,
as they can be used to install dependencies.


django
------

https://www.djangoproject.com/

Django is a web framework. It is used to create applications that operate over
the Internet, such as websites and the like.

Django documentation is really good, so the best way to learn it is going
through the tutorial at:

https://docs.djangoproject.com/en/1.6/intro/tutorial01/

Generally speaking, django is composed of the following components:

* models - Store data and application logic. Models are saved in a database and
  are manipulated using an object-relational mapper. This means that Django
  takes care of writing the actual SQL code to fetch data from the database
  while we can operate on them as regular objects and methods;
* views - Retrieve data from models and process it to produce the requested
  information. Views are normal Python functions that usually fetch model
  objects, do something with them and then return a result as a form of
  a rendered template.
* urlconfs - Do the matching between URLs and the view functions. Urlconfs take
  care of matching specific URLs with the respective view functions that fetch
  the actual data from the models.
* templates - Present the resulting information to the user. Templates use the
  returned data from views and present it to the user, with nicely formatted
  fonts, colors, etc.

Installation
............

Operation
............



Other libraries
---------------

* requests
* pyxb

#Session #1 - giosystem_settings_django application
#::::::::::::::::::::::::::::::::::::::::::::::::::
#
#*to be written*
#
#Session #2 - giosystemcore library
#::::::::::::::::::::::::::::::::::
#
#*to be written*
#
#Session #3 - giosystem-processing components
#::::::::::::::::::::::::::::::::::::::::::::
#
#*to be written*
#
#Session #4 - giosystem-ecflow integration
#:::::::::::::::::::::::::::::::::::::::::
#
#*to be written*
#
#Session #5 - giosystem ordering service
#:::::::::::::::::::::::::::::::::::::::
#
#*to be written*

