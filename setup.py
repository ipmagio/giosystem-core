from setuptools import setup, find_packages
execfile('giosystemcore/version.py')

setup(
    name='giosystemcore',
    version=__version__,
    description='',
    long_description='',
    author='Ricardo Silva',
    author_email='ricardo.silva@ipma.pt',
    url='',
    classifiers=[''],
    platforms=[''],
    license='',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'install_giosystem_hdf5 = giosystemcore.scripts.' \
                'installhdf5:main',
            'install_giosystem_algorithms = giosystemcore.' \
                'scripts.installalgorithms:main',
            'fetch_copsystem_inputs = giosystemcore.scripts.fetchinputs:main',
        ],
    },
    include_package_data=True,
    install_requires=[
        'ecdsa',
        'paramiko',
        'pexpect',
        'pycrypto',
        'pysftp',
        'requests',
        'python-dateutil',
        'six',
        'wsgiref',
        'pystache',
        'owslib',
        'Pillow',
        'pycountry',
        'sphinx_rtd_theme',
        'pytest',
    ]
)
