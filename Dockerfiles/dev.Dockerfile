FROM ricardogsilva/giosystem-core:latest

WORKDIR /usr/src/giosystem-core

COPY . .

RUN pip install --requirement requirements/dev.txt
