FROM ubuntu:16.04

MAINTAINER Ricardo Garcia Silva

LABEL Description="This image builds giosystem-core"

WORKDIR /usr/src/giosystem-core
COPY . .

RUN apt-get update && apt-get install --yes \
        build-essential \
        cgi-mapserver \
        gdal-bin \
        git \
        libgdal-dev \
        libffi-dev \
        libxml2 \
        libxml2-dev \
        libxslt1.1 \
        libxslt1-dev \
        python \
        python-dev \
        python-gdal \
        python-mapscript \
        python-numpy \
        python-numexpr \
        python-h5py \
        wget \
    # update pip
    && wget https://bootstrap.pypa.io/get-pip.py \
    && python get-pip.py \
    # install giosystem-core python dependencies
    && pip install --upgrade \
        pip \
        setuptools \
        pyOpenSSL \
        requests[security] \
    && pip install --requirement requirements/base.txt \
    && pip install --editable . \
    # remove unneeded stuff
    && apt-get purge --yes \
        libgdal-dev \
        libffi-dev \
        libxml2-dev \
        libxslt1-dev \
        python-dev \
        wget \
    && apt-get autoremove --yes \
    &&apt-get clean
